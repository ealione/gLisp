#include "../translator.h"

int main(int argc, char **argv) {

    translator_result_t r;

    translator_parser_t* Adjective = translator_new("adjective");
    translator_parser_t* Noun      = translator_new("noun");
    translator_parser_t* Phrase    = translator_new("phrase");
    translator_parser_t* Doge      = translator_new("doge");

    translatora_lang(TRANSLATOR_LANG_DEFAULT,
              " adjective : \"wow\" | \"many\" | \"so\" | \"such\";                 "
                      " noun      : \"lisp\" | \"language\" | \"c\" | \"book\" | \"build\"; "
                      " phrase    : <adjective> <noun>;                                     "
                      " doge      : /^/ <phrase>* /$/;                                      ",
              Adjective, Noun, Phrase, Doge, NULL);

    if (argc > 1) {

        if (translator_parse_contents(argv[1], Doge, &r)) {
            translator_ast_print(r.output);
            translator_ast_delete(r.output);
        } else {
            translator_err_print(r.error);
            translator_err_delete(r.error);
        }

    } else {

        if (translator_parse_pipe("<stdin>", stdin, Doge, &r)) {
            translator_ast_print(r.output);
            translator_ast_delete(r.output);
        } else {
            translator_err_print(r.error);
            translator_err_delete(r.error);
        }

    }

    translator_cleanup(4, Adjective, Noun, Phrase, Doge);

    return 0;

}
