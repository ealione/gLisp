#include "../translator.h"

int main(int argc, char** argv) {

    translator_result_t r;
    translator_parser_t* Foobar;

    if (argc != 2) {
        printf("Usage: ./foobar <foo/bar>\n");
        exit(0);
    }

    Foobar = translator_new("foobar");
    translatora_lang(TRANSLATOR_LANG_DEFAULT, "foobar : \"foo\" | \"bar\";", Foobar);

    if (translator_parse("<stdin>", argv[1], Foobar, &r)) {
        translator_ast_print(r.output);
        translator_ast_delete(r.output);
    } else {
        translator_err_print(r.error);
        translator_err_delete(r.error);
    }

    translator_cleanup(1, Foobar);

    return 0;
}
