#include "../translator.h"

int main(int argc, char **argv) {

  translator_parser_t* Ident     = translator_new("ident");
  translator_parser_t* Number    = translator_new("number");
  translator_parser_t* Character = translator_new("character");
  translator_parser_t* String    = translator_new("string");
  translator_parser_t* Factor    = translator_new("factor");
  translator_parser_t* Term      = translator_new("term");
  translator_parser_t* Lexp      = translator_new("lexp");
  translator_parser_t* Stmt      = translator_new("stmt");
  translator_parser_t* Exp       = translator_new("exp");
  translator_parser_t* Typeident = translator_new("typeident");
  translator_parser_t* Decls     = translator_new("decls");
  translator_parser_t* Args      = translator_new("args");
  translator_parser_t* Body      = translator_new("body");
  translator_parser_t* Procedure = translator_new("procedure");
  translator_parser_t* Main      = translator_new("main");
  translator_parser_t* Includes  = translator_new("includes");
  translator_parser_t* Smallc    = translator_new("smallc");

  translator_err_t* err = translatora_lang(MPCA_LANG_DEFAULT,
    " ident     : /[a-zA-Z_][a-zA-Z0-9_]*/ ;                           \n"
    " number    : /[0-9]+/ ;                                           \n"
    " character : /'.'/ ;                                              \n"
    " string    : /\"(\\\\.|[^\"])*\"/ ;                               \n"
    "                                                                  \n"
    " factor    : '(' <lexp> ')'                                       \n"
    "           | <number>                                             \n"
    "           | <character>                                          \n"
    "           | <string>                                             \n"
    "           | <ident> '(' <lexp>? (',' <lexp>)* ')'                \n"
    "           | <ident> ;                                            \n"
    "                                                                  \n"
    " term      : <factor> (('*' | '/' | '%') <factor>)* ;             \n"
    " lexp      : <term> (('+' | '-') <term>)* ;                       \n"
    "                                                                  \n"
    " stmt      : '{' <stmt>* '}'                                      \n"
    "           | \"while\" '(' <exp> ')' <stmt>                       \n"
    "           | \"if\"    '(' <exp> ')' <stmt>                       \n"
    "           | <ident> '=' <lexp> ';'                               \n"
    "           | \"print\" '(' <lexp>? ')' ';'                        \n"
    "           | \"return\" <lexp>? ';'                               \n"
    "           | <ident> '(' <ident>? (',' <ident>)* ')' ';' ;        \n"
    "                                                                  \n"
    " exp       : <lexp> '>' <lexp>                                    \n"
    "           | <lexp> '<' <lexp>                                    \n"
    "           | <lexp> \">=\" <lexp>                                 \n"
    "           | <lexp> \"<=\" <lexp>                                 \n"
    "           | <lexp> \"!=\" <lexp>                                 \n"
    "           | <lexp> \"==\" <lexp> ;                               \n"
    "                                                                  \n"
    " typeident : (\"int\" | \"char\") <ident> ;                       \n"
    " decls     : (<typeident> ';')* ;                                 \n"
    " args      : <typeident>? (',' <typeident>)* ;                    \n"
    " body      : '{' <decls> <stmt>* '}' ;                            \n"
    " procedure : (\"int\" | \"char\") <ident> '(' <args> ')' <body> ; \n"
    " main      : \"main\" '(' ')' <body> ;                            \n"
    " includes  : (\"#include\" <string>)* ;                           \n"
    " smallc    : /^/ <includes> <decls> <procedure>* <main> /$/ ;     \n",
    Ident, Number, Character, String, Factor, Term, Lexp, Stmt, Exp, 
    Typeident, Decls, Args, Body, Procedure, Main, Includes, Smallc, NULL);
  
  if (err != NULL) {
    translator_err_print(err);
    translator_err_delete(err);
    exit(1);
  }
    
  if (argc > 1) {
    
    translator_result_t r;
    if (translator_parse_contents(argv[1], Smallc, &r)) {
      translator_ast_print(r.output);
      translator_ast_delete(r.output);
    } else {
      translator_err_print(r.error);
      translator_err_delete(r.error);
    }
    
  } else {
    
    translator_result_t r;
    if (translator_parse_pipe("<stdin>", stdin, Smallc, &r)) {
      translator_ast_print(r.output);
      translator_ast_delete(r.output);
    } else {
      translator_err_print(r.error);
      translator_err_delete(r.error);
    }
  
  }

  translator_cleanup(17, Ident, Number, Character, String, Factor, Term, Lexp, Stmt, Exp,
                  Typeident, Decls, Args, Body, Procedure, Main, Includes, Smallc);
  
  return 0;
  
}

