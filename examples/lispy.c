#include "../translator.h"

int main(int argc, char **argv) {
  
  translator_result_t r;  
  
  translator_parser_t* Number  = translator_new("number");
  translator_parser_t* Symbol  = translator_new("symbol");
  translator_parser_t* String  = translator_new("string");
  translator_parser_t* Comment = translator_new("comment");
  translator_parser_t* Sexpr   = translator_new("sexpr");
  translator_parser_t* Qexpr   = translator_new("qexpr");
  translator_parser_t* Expr    = translator_new("expr");
  translator_parser_t* Lispy   = translator_new("lispy");

  translatora_lang(MPCA_LANG_PREDICTIVE,
    " number  \"number\"  : /[0-9]+/ ;                         "
    " symbol  \"symbol\"  : /[a-zA-Z0-9_+\\-*\\/\\\\=<>!&]+/ ; "
    " string  \"string\"  : /\"(\\\\.|[^\"])*\"/ ;             "
    " comment             : /;[^\\r\\n]*/ ;                    "
    " sexpr               : '(' <expr>* ')' ;                  "
    " qexpr               : '{' <expr>* '}' ;                  "
    " expr                : <number>  | <symbol> | <string>    "
    "                     | <comment> | <sexpr>  | <qexpr> ;   "
    " lispy               : /^/ <expr>* /$/ ;                  ",
    Number, Symbol, String, Comment, Sexpr, Qexpr, Expr, Lispy, NULL);
  
  if (argc > 1) {

    if (translator_parse_contents(argv[1], Lispy, &r)) {
      translator_ast_print(r.output);
      translator_ast_delete(r.output);
    } else {
      translator_err_print(r.error);
      translator_err_delete(r.error);
    }
        
  } else {
    
    if (translator_parse_pipe("<stdin>", stdin, Lispy, &r)) {
      translator_ast_print(r.output);
      translator_ast_delete(r.output);
    } else {
      translator_err_print(r.error);
      translator_err_delete(r.error);
    }
  
  }

  translator_cleanup(8, Number, Symbol, String, Comment, Sexpr, Qexpr, Expr, Lispy);
  
  return 0;
  
}

