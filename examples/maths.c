#include "../translator.h"

int main(int argc, char **argv) {
  
  translator_parser_t *Expr  = translator_new("expression");
  translator_parser_t *Prod  = translator_new("product");
  translator_parser_t *Value = translator_new("value");
  translator_parser_t *Maths = translator_new("maths");
  
  translatora_lang(MPCA_LANG_PREDICTIVE,
    " expression : <product> (('+' | '-') <product>)*; "
    " product : <value>   (('*' | '/')   <value>)*;    "
    " value : /[0-9]+/ | '(' <expression> ')';         "
    " maths : /^/ <expression> /$/;                    ",
    Expr, Prod, Value, Maths, NULL);
  
  translator_print(Expr);
  translator_print(Prod);
  translator_print(Value);
  translator_print(Maths);
  
  if (argc > 1) {
    
    translator_result_t r;
    if (translator_parse_contents(argv[1], Maths, &r)) {
      translator_ast_print(r.output);
      translator_ast_delete(r.output);
    } else {
      translator_err_print(r.error);
      translator_err_delete(r.error);
    }
    
  } else {

    translator_result_t r;
    if (translator_parse_pipe("<stdin>", stdin, Maths, &r)) {
      translator_ast_print(r.output);
      translator_ast_delete(r.output);
    } else {
      translator_err_print(r.error);
      translator_err_delete(r.error);
    }
  
  }

  translator_cleanup(4, Expr, Prod, Value, Maths);
  
  return 0;
  
}

