#ifndef GLISP_TRANSLATOR_H
#define GLISP_TRANSLATOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <ctype.h>

/*
** State Type
*/

typedef struct {
    long pos;
    long row;
    long col;
} translator_state_t;

/*
** Error Type
*/

typedef struct {
    translator_state_t state;
    int expected_num;
    char *filename;
    char *failure;
    char **expected;
    char received;
} translator_err_t;

void translator_err_delete(translator_err_t *e);
char *translator_err_string(translator_err_t *e);
void translator_err_print(translator_err_t *e);
void translator_err_print_to(translator_err_t *e, FILE *f);

/*
** Parsing
*/

typedef void translator_val_t;

typedef union {
    translator_err_t *error;
    translator_val_t *output;
} translator_result_t;

struct translator_parser_t;
typedef struct translator_parser_t translator_parser_t;

int translator_parse(const char *filename, const char *string, translator_parser_t *p, translator_result_t *r);
int translator_nparse(const char *filename, const char *string, size_t length, translator_parser_t *p, translator_result_t *r);
int translator_parse_file(const char *filename, FILE *file, translator_parser_t *p, translator_result_t *r);
int translator_parse_pipe(const char *filename, FILE *pipe, translator_parser_t *p, translator_result_t *r);
int translator_parse_contents(const char *filename, translator_parser_t *p, translator_result_t *r);

/*
** Function Types
*/

typedef void(*translator_dtor_t)(translator_val_t*);
typedef translator_val_t*(*translator_ctor_t)(void);

typedef translator_val_t*(*translator_apply_t)(translator_val_t*);
typedef translator_val_t*(*translator_apply_to_t)(translator_val_t*,void*);
typedef translator_val_t*(*translator_fold_t)(int,translator_val_t**);

/*
** Building a Parser
*/

translator_parser_t *translator_new(const char *name);
translator_parser_t *translator_copy(translator_parser_t *a);
translator_parser_t *translator_define(translator_parser_t *p, translator_parser_t *a);
translator_parser_t *translator_undefine(translator_parser_t *p);

void translator_delete(translator_parser_t *p);
void translator_cleanup(int n, ...);

/*
** Basic Parsers
*/

translator_parser_t *translator_any(void);
translator_parser_t *translator_char(char c);
translator_parser_t *translator_range(char s, char e);
translator_parser_t *translator_oneof(const char *s);
translator_parser_t *translator_noneof(const char *s);
translator_parser_t *translator_satisfy(int(*f)(char));
translator_parser_t *translator_string(const char *s);

/*
** Other Parsers
*/

translator_parser_t *translator_pass(void);
translator_parser_t *translator_fail(const char *m);
translator_parser_t *translator_failf(const char *fmt, ...);
translator_parser_t *translator_lift(translator_ctor_t f);
translator_parser_t *translator_lift_val(translator_val_t *x);
translator_parser_t *translator_anchor(int(*f)(char,char));
translator_parser_t *translator_state(void);

/*
** Combinator Parsers
*/

translator_parser_t *translator_expect(translator_parser_t *a, const char *e);
translator_parser_t *translator_expectf(translator_parser_t *a, const char *fmt, ...);
translator_parser_t *translator_apply(translator_parser_t *a, translator_apply_t f);
translator_parser_t *translator_apply_to(translator_parser_t *a, translator_apply_to_t f, void *x);

translator_parser_t *translator_not(translator_parser_t *a, translator_dtor_t da);
translator_parser_t *translator_not_lift(translator_parser_t *a, translator_dtor_t da, translator_ctor_t lf);
translator_parser_t *translator_maybe(translator_parser_t *a);
translator_parser_t *translator_maybe_lift(translator_parser_t *a, translator_ctor_t lf);

translator_parser_t *translator_many(translator_fold_t f, translator_parser_t *a);
translator_parser_t *translator_many1(translator_fold_t f, translator_parser_t *a);
translator_parser_t *translator_count(int n, translator_fold_t f, translator_parser_t *a, translator_dtor_t da);

translator_parser_t *translator_or(int n, ...);
translator_parser_t *translator_and(int n, translator_fold_t f, ...);

translator_parser_t *translator_predictive(translator_parser_t *a);

/*
** Common Parsers
*/

translator_parser_t *translator_eoi(void);
translator_parser_t *translator_soi(void);

translator_parser_t *translator_boundary(void);

translator_parser_t *translator_whitespace(void);
translator_parser_t *translator_whitespaces(void);
translator_parser_t *translator_blank(void);

translator_parser_t *translator_newline(void);
translator_parser_t *translator_tab(void);
translator_parser_t *translator_escape(void);

translator_parser_t *translator_digit(void);
translator_parser_t *translator_hexdigit(void);
translator_parser_t *translator_octdigit(void);
translator_parser_t *translator_digits(void);
translator_parser_t *translator_hexdigits(void);
translator_parser_t *translator_octdigits(void);

translator_parser_t *translator_lower(void);
translator_parser_t *translator_upper(void);
translator_parser_t *translator_alpha(void);
translator_parser_t *translator_underscore(void);
translator_parser_t *translator_alphanum(void);

translator_parser_t *translator_int(void);
translator_parser_t *translator_hex(void);
translator_parser_t *translator_oct(void);
translator_parser_t *translator_number(void);

translator_parser_t *translator_real(void);
translator_parser_t *translator_float(void);

translator_parser_t *translator_char_lit(void);
translator_parser_t *translator_string_lit(void);
translator_parser_t *translator_regex_lit(void);

translator_parser_t *translator_ident(void);

/*
** Useful Parsers
*/

translator_parser_t *translator_startwith(translator_parser_t *a);
translator_parser_t *translator_endwith(translator_parser_t *a, translator_dtor_t da);
translator_parser_t *translator_whole(translator_parser_t *a, translator_dtor_t da);

translator_parser_t *translator_stripl(translator_parser_t *a);
translator_parser_t *translator_stripr(translator_parser_t *a);
translator_parser_t *translator_strip(translator_parser_t *a);
translator_parser_t *translator_tok(translator_parser_t *a);
translator_parser_t *translator_sym(const char *s);
translator_parser_t *translator_total(translator_parser_t *a, translator_dtor_t da);

translator_parser_t *translator_between(translator_parser_t *a, translator_dtor_t ad, const char *o, const char *c);
translator_parser_t *translator_parens(translator_parser_t *a, translator_dtor_t ad);
translator_parser_t *translator_braces(translator_parser_t *a, translator_dtor_t ad);
translator_parser_t *translator_brackets(translator_parser_t *a, translator_dtor_t ad);
translator_parser_t *translator_squares(translator_parser_t *a, translator_dtor_t ad);

translator_parser_t *translator_tok_between(translator_parser_t *a, translator_dtor_t ad, const char *o, const char *c);
translator_parser_t *translator_tok_parens(translator_parser_t *a, translator_dtor_t ad);
translator_parser_t *translator_tok_braces(translator_parser_t *a, translator_dtor_t ad);
translator_parser_t *translator_tok_brackets(translator_parser_t *a, translator_dtor_t ad);
translator_parser_t *translator_tok_squares(translator_parser_t *a, translator_dtor_t ad);

/*
** Common Function Parameters
*/

void translatorf_dtor_null(translator_val_t *x);

translator_val_t *translatorf_ctor_null(void);
translator_val_t *translatorf_ctor_str(void);

translator_val_t *translatorf_free(translator_val_t *x);
translator_val_t *translatorf_int(translator_val_t *x);
translator_val_t *translatorf_hex(translator_val_t *x);
translator_val_t *translatorf_oct(translator_val_t *x);
translator_val_t *translatorf_float(translator_val_t *x);
translator_val_t *translatorf_strtriml(translator_val_t *x);
translator_val_t *translatorf_strtrimr(translator_val_t *x);
translator_val_t *translatorf_strtrim(translator_val_t *x);

translator_val_t *translatorf_escape(translator_val_t *x);
translator_val_t *translatorf_escape_regex(translator_val_t *x);
translator_val_t *translatorf_escape_string_raw(translator_val_t *x);
translator_val_t *translatorf_escape_char_raw(translator_val_t *x);

translator_val_t *translatorf_unescape(translator_val_t *x);
translator_val_t *translatorf_unescape_regex(translator_val_t *x);
translator_val_t *translatorf_unescape_string_raw(translator_val_t *x);
translator_val_t *translatorf_unescape_char_raw(translator_val_t *x);

translator_val_t *translatorf_null(int n, translator_val_t** xs);
translator_val_t *translatorf_fst(int n, translator_val_t** xs);
translator_val_t *translatorf_snd(int n, translator_val_t** xs);
translator_val_t *translatorf_trd(int n, translator_val_t** xs);

translator_val_t *translatorf_fst_free(int n, translator_val_t** xs);
translator_val_t *translatorf_snd_free(int n, translator_val_t** xs);
translator_val_t *translatorf_trd_free(int n, translator_val_t** xs);

translator_val_t *translatorf_strfold(int n, translator_val_t** xs);
translator_val_t *translatorf_maths(int n, translator_val_t** xs);

/*
** Regular Expression Parsers
*/

translator_parser_t *translator_re(const char *re);

/*
** AST
*/

typedef struct translator_ast_t {
    char *tag;
    char *contents;
    translator_state_t state;
    int children_num;
    struct translator_ast_t** children;
} translator_ast_t;

translator_ast_t *translator_ast_new(const char *tag, const char *contents);
translator_ast_t *translator_ast_build(int n, const char *tag, ...);
translator_ast_t *translator_ast_add_root(translator_ast_t *a);
translator_ast_t *translator_ast_add_child(translator_ast_t *r, translator_ast_t *a);
translator_ast_t *translator_ast_add_tag(translator_ast_t *a, const char *t);
translator_ast_t *translator_ast_add_root_tag(translator_ast_t *a, const char *t);
translator_ast_t *translator_ast_tag(translator_ast_t *a, const char *t);
translator_ast_t *translator_ast_state(translator_ast_t *a, translator_state_t s);

void translator_ast_delete(translator_ast_t *a);
void translator_ast_print(translator_ast_t *a);
void translator_ast_print_to(translator_ast_t *a, FILE *fp);

int translator_ast_get_index(translator_ast_t *ast, const char *tag);
int translator_ast_get_index_lb(translator_ast_t *ast, const char *tag, int lb);
translator_ast_t *translator_ast_get_child(translator_ast_t *ast, const char *tag);
translator_ast_t *translator_ast_get_child_lb(translator_ast_t *ast, const char *tag, int lb);

typedef enum {
    translator_ast_trav_order_pre,
    translator_ast_trav_order_post
} translator_ast_trav_order_t;

typedef struct translator_ast_trav_t {
    translator_ast_t             *curr_node;
    struct translator_ast_trav_t *parent;
    int                    curr_child;
    translator_ast_trav_order_t   order;
} translator_ast_trav_t;

translator_ast_trav_t *translator_ast_traverse_start(translator_ast_t *ast,
                                       translator_ast_trav_order_t order);

translator_ast_t *translator_ast_traverse_next(translator_ast_trav_t **trav);

void translator_ast_traverse_free(translator_ast_trav_t **trav);

/*
** Warning: This function currently doesn't test for equality of the `state` member!
*/
int translator_ast_eq(translator_ast_t *a, translator_ast_t *b);

translator_val_t *translatorf_fold_ast(int n, translator_val_t **as);
translator_val_t *translatorf_str_ast(translator_val_t *c);
translator_val_t *translatorf_state_ast(int n, translator_val_t **xs);

translator_parser_t *translatora_tag(translator_parser_t *a, const char *t);
translator_parser_t *translatora_add_tag(translator_parser_t *a, const char *t);
translator_parser_t *translatora_root(translator_parser_t *a);
translator_parser_t *translatora_state(translator_parser_t *a);
translator_parser_t *translatora_total(translator_parser_t *a);

translator_parser_t *translatora_not(translator_parser_t *a);
translator_parser_t *translatora_maybe(translator_parser_t *a);

translator_parser_t *translatora_many(translator_parser_t *a);
translator_parser_t *translatora_many1(translator_parser_t *a);
translator_parser_t *translatora_count(int n, translator_parser_t *a);

translator_parser_t *translatora_or(int n, ...);
translator_parser_t *translatora_and(int n, ...);

enum {
    TRANSLATOR_LANG_DEFAULT              = 0,
    TRANSLATOR_LANG_PREDICTIVE           = 1,
    TRANSLATOR_LANG_WHITESPACE_SENSITIVE = 2
};

translator_parser_t *translatora_grammar(int flags, const char *grammar, ...);

translator_err_t *translatora_lang(int flags, const char *language, ...);
translator_err_t *translatora_lang_file(int flags, FILE *f, ...);
translator_err_t *translatora_lang_pipe(int flags, FILE *f, ...);
translator_err_t *translatora_lang_contents(int flags, const char *filename, ...);

/*
** Misc
*/


void translator_print(translator_parser_t *p);
void translator_optimise(translator_parser_t *p);
void translator_stats(translator_parser_t *p);

int translator_test_pass(translator_parser_t *p, const char *s, const void *d,
                  int(*tester)(const void*, const void*),
                  translator_dtor_t destructor,
                  void(*printer)(const void*));

int translator_test_fail(translator_parser_t *p, const char *s, const void *d,
                  int(*tester)(const void*, const void*),
                  translator_dtor_t destructor,
                  void(*printer)(const void*));

#ifdef __cplusplus
}
#endif

#endif // GLISP_PARSER_H
