#include "translator.h"

/*
** State Type
*/

static translator_state_t translator_state_invalid(void) {
    translator_state_t s;
    s.pos = -1;
    s.row = -1;
    s.col = -1;
    return s;
}

static translator_state_t translator_state_new(void) {
    translator_state_t s;
    s.pos = 0;
    s.row = 0;
    s.col = 0;
    return s;
}

/*
** Input Type
*/

/*
** In translator the input type has three modes of
** operation: String, File and Pipe.
**
** String is easy. The whole contents are
** loaded into a buffer and scanned through.
** The cursor can jump around at will making
** backtracking easy.
**
** The second is a File which is also somewhat
** easy. The contents are never loaded into
** memory but backtracking can still be achieved
** by seeking in the file at different positions.
**
** The final mode is Pipe. This is the difficult
** one. As we assume pipes cannot be seeked - and
** only support a single character lookahead at
** any point, when the input is marked for a
** potential backtracking we start buffering any
** input.
**
** This means that if we are requested to seek
** back we can simply start reading from the
** buffer instead of the input.
**
** Of course using `translator_predictive` will disable
** backtracking and make LL(1) grammars easy
** to parse for all input methods.
**
*/

enum {
    TRANSLATOR_INPUT_STRING = 0,
    TRANSLATOR_INPUT_FILE = 1,
    TRANSLATOR_INPUT_PIPE = 2
};

enum {
    TRANSLATOR_INPUT_MARKS_MIN = 32
};

enum {
    TRANSLATOR_INPUT_MEM_NUM = 512
};

typedef struct {
    char mem[64];
} translator_mem_t;

typedef struct {

    int type;
    char *filename;
    translator_state_t state;

    char *string;
    char *buffer;
    FILE *file;

    int suppress;
    int backtrack;
    int marks_slots;
    int marks_num;
    translator_state_t *marks;

    char *lasts;
    char last;

    size_t mem_index;
    char mem_full[TRANSLATOR_INPUT_MEM_NUM];
    translator_mem_t mem[TRANSLATOR_INPUT_MEM_NUM];

} translator_input_t;

static translator_input_t *translator_input_new_string(const char *filename, const char *string) {

    translator_input_t *i = malloc(sizeof(translator_input_t));

    i->filename = malloc(strlen(filename) + 1);
    strcpy(i->filename, filename);
    i->type = TRANSLATOR_INPUT_STRING;

    i->state = translator_state_new();

    i->string = malloc(strlen(string) + 1);
    strcpy(i->string, string);
    i->buffer = NULL;
    i->file = NULL;

    i->suppress = 0;
    i->backtrack = 1;
    i->marks_num = 0;
    i->marks_slots = TRANSLATOR_INPUT_MARKS_MIN;
    i->marks = malloc(sizeof(translator_state_t) * i->marks_slots);
    i->lasts = malloc(sizeof(char) * i->marks_slots);
    i->last = '\0';

    i->mem_index = 0;
    memset(i->mem_full, 0, sizeof(char) * TRANSLATOR_INPUT_MEM_NUM);

    return i;
}

static translator_input_t *translator_input_new_nstring(const char *filename, const char *string, size_t length) {

    translator_input_t *i = malloc(sizeof(translator_input_t));

    i->filename = malloc(strlen(filename) + 1);
    strcpy(i->filename, filename);
    i->type = TRANSLATOR_INPUT_STRING;

    i->state = translator_state_new();

    i->string = malloc(length + 1);
    strncpy(i->string, string, length);
    i->string[length] = '\0';
    i->buffer = NULL;
    i->file = NULL;

    i->suppress = 0;
    i->backtrack = 1;
    i->marks_num = 0;
    i->marks_slots = TRANSLATOR_INPUT_MARKS_MIN;
    i->marks = malloc(sizeof(translator_state_t) * i->marks_slots);
    i->lasts = malloc(sizeof(char) * i->marks_slots);
    i->last = '\0';

    i->mem_index = 0;
    memset(i->mem_full, 0, sizeof(char) * TRANSLATOR_INPUT_MEM_NUM);

    return i;
}

static translator_input_t *translator_input_new_pipe(const char *filename, FILE *pipe) {

    translator_input_t *i = malloc(sizeof(translator_input_t));

    i->filename = malloc(strlen(filename) + 1);
    strcpy(i->filename, filename);

    i->type = TRANSLATOR_INPUT_PIPE;
    i->state = translator_state_new();

    i->string = NULL;
    i->buffer = NULL;
    i->file = pipe;

    i->suppress = 0;
    i->backtrack = 1;
    i->marks_num = 0;
    i->marks_slots = TRANSLATOR_INPUT_MARKS_MIN;
    i->marks = malloc(sizeof(translator_state_t) * i->marks_slots);
    i->lasts = malloc(sizeof(char) * i->marks_slots);
    i->last = '\0';

    i->mem_index = 0;
    memset(i->mem_full, 0, sizeof(char) * TRANSLATOR_INPUT_MEM_NUM);

    return i;
}

static translator_input_t *translator_input_new_file(const char *filename, FILE *file) {

    translator_input_t *i = malloc(sizeof(translator_input_t));

    i->filename = malloc(strlen(filename) + 1);
    strcpy(i->filename, filename);
    i->type = TRANSLATOR_INPUT_FILE;
    i->state = translator_state_new();

    i->string = NULL;
    i->buffer = NULL;
    i->file = file;

    i->suppress = 0;
    i->backtrack = 1;
    i->marks_num = 0;
    i->marks_slots = TRANSLATOR_INPUT_MARKS_MIN;
    i->marks = malloc(sizeof(translator_state_t) * i->marks_slots);
    i->lasts = malloc(sizeof(char) * i->marks_slots);
    i->last = '\0';

    i->mem_index = 0;
    memset(i->mem_full, 0, sizeof(char) * TRANSLATOR_INPUT_MEM_NUM);

    return i;
}

static void translator_input_delete(translator_input_t *i) {

    free(i->filename);

    if (i->type == TRANSLATOR_INPUT_STRING) { free(i->string); }
    if (i->type == TRANSLATOR_INPUT_PIPE) { free(i->buffer); }

    free(i->marks);
    free(i->lasts);
    free(i);
}

static int translator_mem_ptr(translator_input_t *i, void *p) {
    return
            (char *) p >= (char *) (i->mem) &&
            (char *) p < (char *) (i->mem) + (TRANSLATOR_INPUT_MEM_NUM * sizeof(translator_mem_t));
}

static void *translator_malloc(translator_input_t *i, size_t n) {
    size_t j;
    char *p;

    if (n > sizeof(translator_mem_t)) { return malloc(n); }

    j = i->mem_index;
    do {
        if (!i->mem_full[i->mem_index]) {
            p = (void *) (i->mem + i->mem_index);
            i->mem_full[i->mem_index] = 1;
            i->mem_index = (i->mem_index + 1) % TRANSLATOR_INPUT_MEM_NUM;
            return p;
        }
        i->mem_index = (i->mem_index + 1) % TRANSLATOR_INPUT_MEM_NUM;
    } while (j != i->mem_index);

    return malloc(n);
}

static void *translator_calloc(translator_input_t *i, size_t n, size_t m) {
    char *x = translator_malloc(i, n * m);
    memset(x, 0, n * m);
    return x;
}

static void translator_free(translator_input_t *i, void *p) {
    size_t j;
    if (!translator_mem_ptr(i, p)) {
        free(p);
        return;
    }
    j = ((size_t) (((char *) p) - ((char *) i->mem))) / sizeof(translator_mem_t);
    i->mem_full[j] = 0;
}

static void *translator_realloc(translator_input_t *i, void *p, size_t n) {

    char *q = NULL;

    if (!translator_mem_ptr(i, p)) { return realloc(p, n); }

    if (n > sizeof(translator_mem_t)) {
        q = malloc(n);
        memcpy(q, p, sizeof(translator_mem_t));
        translator_free(i, p);
        return q;
    }

    return p;
}

static void *translator_export(translator_input_t *i, void *p) {
    char *q = NULL;
    if (!translator_mem_ptr(i, p)) { return p; }
    q = malloc(sizeof(translator_mem_t));
    memcpy(q, p, sizeof(translator_mem_t));
    translator_free(i, p);
    return q;
}

static void translator_input_backtrack_disable(translator_input_t *i) { i->backtrack--; }

static void translator_input_backtrack_enable(translator_input_t *i) { i->backtrack++; }

static void translator_input_suppress_disable(translator_input_t *i) { i->suppress--; }

static void translator_input_suppress_enable(translator_input_t *i) { i->suppress++; }

static void translator_input_mark(translator_input_t *i) {

    if (i->backtrack < 1) { return; }

    i->marks_num++;

    if (i->marks_num > i->marks_slots) {
        i->marks_slots = i->marks_num + i->marks_num / 2;
        i->marks = realloc(i->marks, sizeof(translator_state_t) * i->marks_slots);
        i->lasts = realloc(i->lasts, sizeof(char) * i->marks_slots);
    }

    i->marks[i->marks_num - 1] = i->state;
    i->lasts[i->marks_num - 1] = i->last;

    if (i->type == TRANSLATOR_INPUT_PIPE && i->marks_num == 1) {
        i->buffer = calloc(1, 1);
    }
}

static void translator_input_unmark(translator_input_t *i) {

    if (i->backtrack < 1) { return; }

    i->marks_num--;

    if (i->marks_slots > i->marks_num + i->marks_num / 2
        && i->marks_slots > TRANSLATOR_INPUT_MARKS_MIN) {
        i->marks_slots =
                i->marks_num > TRANSLATOR_INPUT_MARKS_MIN ?
                i->marks_num : TRANSLATOR_INPUT_MARKS_MIN;
        i->marks = realloc(i->marks, sizeof(translator_state_t) * i->marks_slots);
        i->lasts = realloc(i->lasts, sizeof(char) * i->marks_slots);
    }

    if (i->type == TRANSLATOR_INPUT_PIPE && i->marks_num == 0) {
        free(i->buffer);
        i->buffer = NULL;
    }
}

static void translator_input_rewind(translator_input_t *i) {

    if (i->backtrack < 1) { return; }

    i->state = i->marks[i->marks_num - 1];
    i->last = i->lasts[i->marks_num - 1];

    if (i->type == TRANSLATOR_INPUT_FILE) {
        fseek(i->file, i->state.pos, SEEK_SET);
    }

    translator_input_unmark(i);
}

static int translator_input_buffer_in_range(translator_input_t *i) {
    return i->state.pos < (long) (strlen(i->buffer) + i->marks[0].pos);
}

static char translator_input_buffer_get(translator_input_t *i) {
    return i->buffer[i->state.pos - i->marks[0].pos];
}

static int translator_input_terminated(translator_input_t *i) {
    if (i->type == TRANSLATOR_INPUT_STRING && i->state.pos == (long) strlen(i->string)) { return 1; }
    if (i->type == TRANSLATOR_INPUT_FILE && feof(i->file)) { return 1; }
    if (i->type == TRANSLATOR_INPUT_PIPE && feof(i->file)) { return 1; }
    return 0;
}

static char translator_input_getc(translator_input_t *i) {

    char c = '\0';

    switch (i->type) {

        case TRANSLATOR_INPUT_STRING:
            return i->string[i->state.pos];
        case TRANSLATOR_INPUT_FILE:
            c = (char) fgetc(i->file);
            return c;
        case TRANSLATOR_INPUT_PIPE:

            if (!i->buffer) {
                c = (char) getc(i->file);
                return c;
            }

            if (i->buffer && translator_input_buffer_in_range(i)) {
                c = translator_input_buffer_get(i);
                return c;
            } else {
                c = (char) getc(i->file);
                return c;
            }

        default:
            return c;
    }
}

static char translator_input_peekc(translator_input_t *i) {

    char c = '\0';

    switch (i->type) {
        case TRANSLATOR_INPUT_STRING:
            return i->string[i->state.pos];
        case TRANSLATOR_INPUT_FILE:

            c = (char) fgetc(i->file);
            if (feof(i->file)) { return '\0'; }

            fseek(i->file, -1, SEEK_CUR);
            return c;

        case TRANSLATOR_INPUT_PIPE:

            if (!i->buffer) {
                c = (char) getc(i->file);
                if (feof(i->file)) { return '\0'; }
                ungetc(c, i->file);
                return c;
            }

            if (i->buffer && translator_input_buffer_in_range(i)) {
                return translator_input_buffer_get(i);
            } else {
                c = (char) getc(i->file);
                if (feof(i->file)) { return '\0'; }
                ungetc(c, i->file);
                return c;
            }

        default:
            return c;
    }
}

static int translator_input_failure(translator_input_t *i, char c) {

    switch (i->type) {
        case TRANSLATOR_INPUT_STRING: {
            break;
        }
        case TRANSLATOR_INPUT_FILE:
            fseek(i->file, -1, SEEK_CUR);
            { break; }
        case TRANSLATOR_INPUT_PIPE: {

            if (!i->buffer) {
                ungetc(c, i->file);
                break;
            }

            if (i->buffer && translator_input_buffer_in_range(i)) {
                break;
            } else {
                ungetc(c, i->file);
            }
        }
        default: {
            break;
        }
    }
    return 0;
}

static int translator_input_success(translator_input_t *i, char c, char **o) {

    if (i->type == TRANSLATOR_INPUT_PIPE
        && i->buffer && !translator_input_buffer_in_range(i)) {
        i->buffer = realloc(i->buffer, strlen(i->buffer) + 2);
        i->buffer[strlen(i->buffer) + 1] = '\0';
        i->buffer[strlen(i->buffer) + 0] = c;
    }

    i->last = c;
    i->state.pos++;
    i->state.col++;

    if (c == '\n') {
        i->state.col = 0;
        i->state.row++;
    }

    if (o) {
        (*o) = translator_malloc(i, 2);
        (*o)[0] = c;
        (*o)[1] = '\0';
    }

    return 1;
}

static int translator_input_any(translator_input_t *i, char **o) {
    char x = translator_input_getc(i);
    if (translator_input_terminated(i)) { return 0; }
    return translator_input_success(i, x, o);
}

static int translator_input_char(translator_input_t *i, char c, char **o) {
    char x = translator_input_getc(i);
    if (translator_input_terminated(i)) { return 0; }
    return x == c ? translator_input_success(i, x, o) : translator_input_failure(i, x);
}

static int translator_input_range(translator_input_t *i, char c, char d, char **o) {
    char x = translator_input_getc(i);
    if (translator_input_terminated(i)) { return 0; }
    return x >= c && x <= d ? translator_input_success(i, x, o) : translator_input_failure(i, x);
}

static int translator_input_oneof(translator_input_t *i, const char *c, char **o) {
    char x = translator_input_getc(i);
    if (translator_input_terminated(i)) { return 0; }
    return strchr(c, x) != 0 ? translator_input_success(i, x, o) : translator_input_failure(i, x);
}

static int translator_input_noneof(translator_input_t *i, const char *c, char **o) {
    char x = translator_input_getc(i);
    if (translator_input_terminated(i)) { return 0; }
    return strchr(c, x) == 0 ? translator_input_success(i, x, o) : translator_input_failure(i, x);
}

static int translator_input_satisfy(translator_input_t *i, int(*cond)(char), char **o) {
    char x = translator_input_getc(i);
    if (translator_input_terminated(i)) { return 0; }
    return cond(x) ? translator_input_success(i, x, o) : translator_input_failure(i, x);
}

static int translator_input_string(translator_input_t *i, const char *c, char **o) {

    const char *x = c;

    translator_input_mark(i);
    while (*x) {
        if (!translator_input_char(i, *x, NULL)) {
            translator_input_rewind(i);
            return 0;
        }
        x++;
    }
    translator_input_unmark(i);

    *o = translator_malloc(i, strlen(c) + 1);
    strcpy(*o, c);
    return 1;
}

static int translator_input_anchor(translator_input_t *i, int(*f)(char, char), char **o) {
    *o = NULL;
    return f(i->last, translator_input_peekc(i));
}

static translator_state_t *translator_input_state_copy(translator_input_t *i) {
    translator_state_t *r = translator_malloc(i, sizeof(translator_state_t));
    memcpy(r, &i->state, sizeof(translator_state_t));
    return r;
}

/*
** Error Type
*/

void translator_err_delete(translator_err_t *x) {
    int i;
    for (i = 0; i < x->expected_num; i++) { free(x->expected[i]); }
    free(x->expected);
    free(x->filename);
    free(x->failure);
    free(x);
}

void translator_err_print(translator_err_t *x) {
    translator_err_print_to(x, stdout);
}

void translator_err_print_to(translator_err_t *x, FILE *f) {
    char *str = translator_err_string(x);
    fprintf(f, "%s", str);
    free(str);
}

static void translator_err_string_cat(char *buffer, int *pos, int *max, char const *fmt, ...) {
    /* TODO: Error Checking on Length */
    int left = ((*max) - (*pos));
    va_list va;
    va_start(va, fmt);
    if (left < 0) { left = 0; }
    (*pos) += vsprintf(buffer + (*pos), fmt, va);
    va_end(va);
}

static char char_unescape_buffer[4];

static const char *translator_err_char_unescape(char c) {

    char_unescape_buffer[0] = '\'';
    char_unescape_buffer[1] = ' ';
    char_unescape_buffer[2] = '\'';
    char_unescape_buffer[3] = '\0';

    switch (c) {
        case '\a':
            return "bell";
        case '\b':
            return "backspace";
        case '\f':
            return "formfeed";
        case '\r':
            return "carriage return";
        case '\v':
            return "vertical tab";
        case '\0':
            return "end of input";
        case '\n':
            return "newline";
        case '\t':
            return "tab";
        case ' ' :
            return "space";
        default:
            char_unescape_buffer[1] = c;
            return char_unescape_buffer;
    }

}

char *translator_err_string(translator_err_t *x) {

    int i;
    int pos = 0;
    int max = 1023;
    char *buffer = calloc(1, 1024);

    if (x->failure) {
        translator_err_string_cat(buffer, &pos, &max,
                                  "%s: error: %s\n", x->filename, x->failure);
        return buffer;
    }

    translator_err_string_cat(buffer, &pos, &max,
                              "%s:%i:%i: error: expected ", x->filename, x->state.row + 1, x->state.col + 1);

    if (x->expected_num == 0) { translator_err_string_cat(buffer, &pos, &max, "ERROR: NOTHING EXPECTED"); }
    if (x->expected_num == 1) { translator_err_string_cat(buffer, &pos, &max, "%s", x->expected[0]); }
    if (x->expected_num >= 2) {

        for (i = 0; i < x->expected_num - 2; i++) {
            translator_err_string_cat(buffer, &pos, &max, "%s, ", x->expected[i]);
        }

        translator_err_string_cat(buffer, &pos, &max, "%s or %s",
                                  x->expected[x->expected_num - 2],
                                  x->expected[x->expected_num - 1]);
    }

    translator_err_string_cat(buffer, &pos, &max, " at ");
    translator_err_string_cat(buffer, &pos, &max, translator_err_char_unescape(x->received));
    translator_err_string_cat(buffer, &pos, &max, "\n");

    return realloc(buffer, strlen(buffer) + 1);
}

static translator_err_t *translator_err_new(translator_input_t *i, const char *expected) {
    translator_err_t *x;
    if (i->suppress) { return NULL; }
    x = translator_malloc(i, sizeof(translator_err_t));
    x->filename = translator_malloc(i, strlen(i->filename) + 1);
    strcpy(x->filename, i->filename);
    x->state = i->state;
    x->expected_num = 1;
    x->expected = translator_malloc(i, sizeof(char *));
    x->expected[0] = translator_malloc(i, strlen(expected) + 1);
    strcpy(x->expected[0], expected);
    x->failure = NULL;
    x->received = translator_input_peekc(i);
    return x;
}

static translator_err_t *translator_err_fail(translator_input_t *i, const char *failure) {
    translator_err_t *x;
    if (i->suppress) { return NULL; }
    x = translator_malloc(i, sizeof(translator_err_t));
    x->filename = translator_malloc(i, strlen(i->filename) + 1);
    strcpy(x->filename, i->filename);
    x->state = i->state;
    x->expected_num = 0;
    x->expected = NULL;
    x->failure = translator_malloc(i, strlen(failure) + 1);
    strcpy(x->failure, failure);
    x->received = ' ';
    return x;
}

static translator_err_t *translator_err_file(const char *filename, const char *failure) {
    translator_err_t *x;
    x = malloc(sizeof(translator_err_t));
    x->filename = malloc(strlen(filename) + 1);
    strcpy(x->filename, filename);
    x->state = translator_state_new();
    x->expected_num = 0;
    x->expected = NULL;
    x->failure = malloc(strlen(failure) + 1);
    strcpy(x->failure, failure);
    x->received = ' ';
    return x;
}

static void translator_err_delete_internal(translator_input_t *i, translator_err_t *x) {
    int j;
    if (x == NULL) { return; }
    for (j = 0; j < x->expected_num; j++) { translator_free(i, x->expected[j]); }
    translator_free(i, x->expected);
    translator_free(i, x->filename);
    translator_free(i, x->failure);
    translator_free(i, x);
}

static translator_err_t *translator_err_export(translator_input_t *i, translator_err_t *x) {
    int j;
    for (j = 0; j < x->expected_num; j++) {
        x->expected[j] = translator_export(i, x->expected[j]);
    }
    x->expected = translator_export(i, x->expected);
    x->filename = translator_export(i, x->filename);
    x->failure = translator_export(i, x->failure);
    return translator_export(i, x);
}

static int translator_err_contains_expected(translator_input_t *i, translator_err_t *x, char *expected) {
    int j;
    (void) i;
    for (j = 0; j < x->expected_num; j++) {
        if (strcmp(x->expected[j], expected) == 0) { return 1; }
    }
    return 0;
}

static void translator_err_add_expected(translator_input_t *i, translator_err_t *x, char *expected) {
    (void) i;
    x->expected_num++;
    x->expected = translator_realloc(i, x->expected, sizeof(char *) * x->expected_num);
    x->expected[x->expected_num - 1] = translator_malloc(i, strlen(expected) + 1);
    strcpy(x->expected[x->expected_num - 1], expected);
}

static translator_err_t *translator_err_or(translator_input_t *i, translator_err_t **x, int n) {

    int j, k, fst;
    translator_err_t *e;

    fst = -1;
    for (j = 0; j < n; j++) {
        if (x[j] != NULL) { fst = j; }
    }

    if (fst == -1) { return NULL; }

    e = translator_malloc(i, sizeof(translator_err_t));
    e->state = translator_state_invalid();
    e->expected_num = 0;
    e->expected = NULL;
    e->failure = NULL;
    e->filename = translator_malloc(i, strlen(x[fst]->filename) + 1);
    strcpy(e->filename, x[fst]->filename);

    for (j = 0; j < n; j++) {
        if (x[j] == NULL) { continue; }
        if (x[j]->state.pos > e->state.pos) { e->state = x[j]->state; }
    }

    for (j = 0; j < n; j++) {
        if (x[j] == NULL) { continue; }
        if (x[j]->state.pos < e->state.pos) { continue; }

        if (x[j]->failure) {
            e->failure = translator_malloc(i, strlen(x[j]->failure) + 1);
            strcpy(e->failure, x[j]->failure);
            break;
        }

        e->received = x[j]->received;

        for (k = 0; k < x[j]->expected_num; k++) {
            if (!translator_err_contains_expected(i, e, x[j]->expected[k])) {
                translator_err_add_expected(i, e, x[j]->expected[k]);
            }
        }
    }

    for (j = 0; j < n; j++) {
        if (x[j] == NULL) { continue; }
        translator_err_delete_internal(i, x[j]);
    }

    return e;
}

static translator_err_t *translator_err_repeat(translator_input_t *i, translator_err_t *x, const char *prefix) {

    int j = 0;
    size_t l = 0;
    char *expect = NULL;

    if (x == NULL) { return NULL; }

    if (x->expected_num == 0) {
        expect = translator_calloc(i, 1, 1);
        x->expected_num = 1;
        x->expected = translator_realloc(i, x->expected, sizeof(char *) * x->expected_num);
        x->expected[0] = expect;
        return x;
    } else if (x->expected_num == 1) {
        expect = translator_malloc(i, strlen(prefix) + strlen(x->expected[0]) + 1);
        strcpy(expect, prefix);
        strcat(expect, x->expected[0]);
        translator_free(i, x->expected[0]);
        x->expected[0] = expect;
        return x;
    } else if (x->expected_num > 1) {

        l += strlen(prefix);
        for (j = 0; j < x->expected_num - 2; j++) {
            l += strlen(x->expected[j]) + strlen(", ");
        }
        l += strlen(x->expected[x->expected_num - 2]);
        l += strlen(" or ");
        l += strlen(x->expected[x->expected_num - 1]);

        expect = translator_malloc(i, l + 1);

        strcpy(expect, prefix);
        for (j = 0; j < x->expected_num - 2; j++) {
            strcat(expect, x->expected[j]);
            strcat(expect, ", ");
        }
        strcat(expect, x->expected[x->expected_num - 2]);
        strcat(expect, " or ");
        strcat(expect, x->expected[x->expected_num - 1]);

        for (j = 0; j < x->expected_num; j++) { translator_free(i, x->expected[j]); }

        x->expected_num = 1;
        x->expected = translator_realloc(i, x->expected, sizeof(char *) * x->expected_num);
        x->expected[0] = expect;
        return x;
    }

    return NULL;
}

static translator_err_t *translator_err_many1(translator_input_t *i, translator_err_t *x) {
    return translator_err_repeat(i, x, "one or more of ");
}

static translator_err_t *translator_err_count(translator_input_t *i, translator_err_t *x, int n) {
    translator_err_t *y;
    int digits = n / 10 + 1;
    char *prefix;
    prefix = translator_malloc(i, digits + strlen(" of ") + 1);
    sprintf(prefix, "%i of ", n);
    y = translator_err_repeat(i, x, prefix);
    translator_free(i, prefix);
    return y;
}

static translator_err_t *translator_err_merge(translator_input_t *i, translator_err_t *x, translator_err_t *y) {
    translator_err_t *errs[2];
    errs[0] = x;
    errs[1] = y;
    return translator_err_or(i, errs, 2);
}

/*
** Parser Type
*/

enum {
    translator_TYPE_UNDEFINED = 0,
    translator_TYPE_PASS = 1,
    translator_TYPE_FAIL = 2,
    translator_TYPE_LIFT = 3,
    translator_TYPE_LIFT_VAL = 4,
    translator_TYPE_EXPECT = 5,
    translator_TYPE_ANCHOR = 6,
    translator_TYPE_STATE = 7,

    translator_TYPE_ANY = 8,
    translator_TYPE_SINGLE = 9,
    translator_TYPE_ONEOF = 10,
    translator_TYPE_NONEOF = 11,
    translator_TYPE_RANGE = 12,
    translator_TYPE_SATISFY = 13,
    translator_TYPE_STRING = 14,

    translator_TYPE_APPLY = 15,
    translator_TYPE_APPLY_TO = 16,
    translator_TYPE_PREDICT = 17,
    translator_TYPE_NOT = 18,
    translator_TYPE_MAYBE = 19,
    translator_TYPE_MANY = 20,
    translator_TYPE_MANY1 = 21,
    translator_TYPE_COUNT = 22,

    translator_TYPE_OR = 23,
    translator_TYPE_AND = 24
};

typedef struct {
    char *m;
} translator_pdata_fail_t;

typedef struct {
    translator_ctor_t lf;
    void *x;
} translator_pdata_lift_t;

typedef struct {
    translator_parser_t *x;
    char *m;
} translator_pdata_expect_t;

typedef struct {
    int (*f)(char, char);
} translator_pdata_anchor_t;

typedef struct {
    char x;
} translator_pdata_single_t;

typedef struct {
    char x;
    char y;
} translator_pdata_range_t;

typedef struct {
    int (*f)(char);
} translator_pdata_satisfy_t;

typedef struct {
    char *x;
} translator_pdata_string_t;

typedef struct {
    translator_parser_t *x;
    translator_apply_t f;
} translator_pdata_apply_t;

typedef struct {
    translator_parser_t *x;
    translator_apply_to_t f;
    void *d;
} translator_pdata_apply_to_t;

typedef struct {
    translator_parser_t *x;
} translator_pdata_predict_t;

typedef struct {
    translator_parser_t *x;
    translator_dtor_t dx;
    translator_ctor_t lf;
} translator_pdata_not_t;

typedef struct {
    int n;
    translator_fold_t f;
    translator_parser_t *x;
    translator_dtor_t dx;
} translator_pdata_repeat_t;

typedef struct {
    int n;
    translator_parser_t **xs;
} translator_pdata_or_t;

typedef struct {
    int n;
    translator_fold_t f;
    translator_parser_t **xs;
    translator_dtor_t *dxs;
} translator_pdata_and_t;

typedef union {
    translator_pdata_fail_t fail;
    translator_pdata_lift_t lift;
    translator_pdata_expect_t expect;
    translator_pdata_anchor_t anchor;
    translator_pdata_single_t single;
    translator_pdata_range_t range;
    translator_pdata_satisfy_t satisfy;
    translator_pdata_string_t string;
    translator_pdata_apply_t apply;
    translator_pdata_apply_to_t apply_to;
    translator_pdata_predict_t predict;
    translator_pdata_not_t not;
    translator_pdata_repeat_t repeat;
    translator_pdata_and_t and;
    translator_pdata_or_t or;
} translator_pdata_t;

struct translator_parser_t {
    char *name;
    translator_pdata_t data;
    char type;
    char retained;
};

static translator_val_t *translatorf_input_nth_free(translator_input_t *i, int n, translator_val_t **xs, int x) {
    int j;
    for (j = 0; j < n; j++) { if (j != x) { translator_free(i, xs[j]); }}
    return xs[x];
}

static translator_val_t *translatorf_input_fst_free(translator_input_t *i, int n, translator_val_t **xs) {
    return translatorf_input_nth_free(i, n, xs, 0);
}

static translator_val_t *translatorf_input_snd_free(translator_input_t *i, int n, translator_val_t **xs) {
    return translatorf_input_nth_free(i, n, xs, 1);
}

static translator_val_t *translatorf_input_trd_free(translator_input_t *i, int n, translator_val_t **xs) {
    return translatorf_input_nth_free(i, n, xs, 2);
}

static translator_val_t *translatorf_input_strfold(translator_input_t *i, int n, translator_val_t **xs) {
    int j;
    size_t l = 0;
    if (n == 0) { return translator_calloc(i, 1, 1); }
    for (j = 0; j < n; j++) { l += strlen(xs[j]); }
    xs[0] = translator_realloc(i, xs[0], l + 1);
    for (j = 1; j < n; j++) {
        strcat(xs[0], xs[j]);
        translator_free(i, xs[j]);
    }
    return xs[0];
}

static translator_val_t *translatorf_input_state_ast(translator_input_t *i, int n, translator_val_t **xs) {
    translator_state_t *s = ((translator_state_t **) xs)[0];
    translator_ast_t *a = ((translator_ast_t **) xs)[1];
    a = translator_ast_state(a, *s);
    translator_free(i, s);
    (void) n;
    return a;
}

static translator_val_t *
translator_parse_fold(translator_input_t *i, translator_fold_t f, int n, translator_val_t **xs) {
    int j;
    if (f == translatorf_null) { return translatorf_null(n, xs); }
    if (f == translatorf_fst) { return translatorf_fst(n, xs); }
    if (f == translatorf_snd) { return translatorf_snd(n, xs); }
    if (f == translatorf_trd) { return translatorf_trd(n, xs); }
    if (f == translatorf_fst_free) { return translatorf_input_fst_free(i, n, xs); }
    if (f == translatorf_snd_free) { return translatorf_input_snd_free(i, n, xs); }
    if (f == translatorf_trd_free) { return translatorf_input_trd_free(i, n, xs); }
    if (f == translatorf_strfold) { return translatorf_input_strfold(i, n, xs); }
    if (f == translatorf_state_ast) { return translatorf_input_state_ast(i, n, xs); }
    for (j = 0; j < n; j++) { xs[j] = translator_export(i, xs[j]); }
    return f(j, xs);
}

static translator_val_t *translatorf_input_free(translator_input_t *i, translator_val_t *x) {
    translator_free(i, x);
    return NULL;
}

static translator_val_t *translatorf_input_str_ast(translator_input_t *i, translator_val_t *c) {
    translator_ast_t *a = translator_ast_new("", c);
    translator_free(i, c);
    return a;
}

static translator_val_t *translator_parse_apply(translator_input_t *i, translator_apply_t f, translator_val_t *x) {
    if (f == translatorf_free) { return translatorf_input_free(i, x); }
    if (f == translatorf_str_ast) { return translatorf_input_str_ast(i, x); }
    return f(translator_export(i, x));
}

static translator_val_t *
translator_parse_apply_to(translator_input_t *i, translator_apply_to_t f, translator_val_t *x, translator_val_t *d) {
    return f(translator_export(i, x), d);
}

static void translator_parse_dtor(translator_input_t *i, translator_dtor_t d, translator_val_t *x) {
    if (d == free) {
        translator_free(i, x);
        return;
    }
    d(translator_export(i, x));
}

enum {
    translator_PARSE_STACK_MIN = 4
};

#define translator_SUCCESS(x) r->output = x; return 1
#define translator_FAILURE(x) r->error = x; return 0
#define translator_PRIMITIVE(x) \
  if (x) { translator_SUCCESS(r->output); } \
  else { translator_FAILURE(NULL); }

static int
translator_parse_run(translator_input_t *i, translator_parser_t *p, translator_result_t *r, translator_err_t **e) {

    int j = 0, k = 0;
    translator_result_t results_stk[translator_PARSE_STACK_MIN];
    translator_result_t *results;
    int results_slots = translator_PARSE_STACK_MIN;

    switch (p->type) {

        /* Basic Parsers */

        case translator_TYPE_ANY:
            translator_PRIMITIVE(translator_input_any(i, (char **) &r->output));
        case translator_TYPE_SINGLE:
            translator_PRIMITIVE(translator_input_char(i, p->data.single.x, (char **) &r->output));
        case translator_TYPE_RANGE:
            translator_PRIMITIVE(translator_input_range(i, p->data.range.x, p->data.range.y, (char **) &r->output));
        case translator_TYPE_ONEOF:
            translator_PRIMITIVE(translator_input_oneof(i, p->data.string.x, (char **) &r->output));
        case translator_TYPE_NONEOF:
            translator_PRIMITIVE(translator_input_noneof(i, p->data.string.x, (char **) &r->output));
        case translator_TYPE_SATISFY:
            translator_PRIMITIVE(translator_input_satisfy(i, p->data.satisfy.f, (char **) &r->output));
        case translator_TYPE_STRING:
            translator_PRIMITIVE(translator_input_string(i, p->data.string.x, (char **) &r->output));
        case translator_TYPE_ANCHOR:
            translator_PRIMITIVE(translator_input_anchor(i, p->data.anchor.f, (char **) &r->output));

            /* Other parsers */

        case translator_TYPE_UNDEFINED:
        translator_FAILURE(translator_err_fail(i, "Parser Undefined!"));
        case translator_TYPE_PASS:
        translator_SUCCESS(NULL);
        case translator_TYPE_FAIL:
        translator_FAILURE(translator_err_fail(i, p->data.fail.m));
        case translator_TYPE_LIFT:
        translator_SUCCESS(p->data.lift.lf());
        case translator_TYPE_LIFT_VAL:
        translator_SUCCESS(p->data.lift.x);
        case translator_TYPE_STATE:
        translator_SUCCESS(translator_input_state_copy(i));

            /* Application Parsers */

        case translator_TYPE_APPLY:
            if (translator_parse_run(i, p->data.apply.x, r, e)) {
                translator_SUCCESS(translator_parse_apply(i, p->data.apply.f, r->output));
            } else {
                translator_FAILURE(r->output);
            }

        case translator_TYPE_APPLY_TO:
            if (translator_parse_run(i, p->data.apply_to.x, r, e)) {
                translator_SUCCESS(translator_parse_apply_to(i, p->data.apply_to.f, r->output, p->data.apply_to.d));
            } else {
                translator_FAILURE(r->error);
            }

        case translator_TYPE_EXPECT:
            translator_input_suppress_enable(i);
            if (translator_parse_run(i, p->data.expect.x, r, e)) {
                translator_input_suppress_disable(i);
                translator_SUCCESS(r->output);
            } else {
                translator_input_suppress_disable(i);
                translator_FAILURE(translator_err_new(i, p->data.expect.m));
            }

        case translator_TYPE_PREDICT:
            translator_input_backtrack_disable(i);
            if (translator_parse_run(i, p->data.predict.x, r, e)) {
                translator_input_backtrack_enable(i);
                translator_SUCCESS(r->output);
            } else {
                translator_input_backtrack_enable(i);
                translator_FAILURE(r->error);
            }

            /* Optional Parsers */

            /* TODO: Update Not Error Message */

        case translator_TYPE_NOT:
            translator_input_mark(i);
            translator_input_suppress_enable(i);
            if (translator_parse_run(i, p->data.not.x, r, e)) {
                translator_input_rewind(i);
                translator_input_suppress_disable(i);
                translator_parse_dtor(i, p->data.not.dx, r->output);
                translator_FAILURE(translator_err_new(i, "opposite"));
            } else {
                translator_input_unmark(i);
                translator_input_suppress_disable(i);
                translator_SUCCESS(p->data.not.lf());
            }

        case translator_TYPE_MAYBE:
            if (translator_parse_run(i, p->data.not.x, r, e)) {
                translator_SUCCESS(r->output);
            } else {
                *e = translator_err_merge(i, *e, r->error);
                translator_SUCCESS(p->data.not.lf());
            }

            /* Repeat Parsers */

        case translator_TYPE_MANY:

            results = results_stk;

            while (translator_parse_run(i, p->data.repeat.x, &results[j], e)) {
                j++;
                if (j == translator_PARSE_STACK_MIN) {
                    results_slots = j + j / 2;
                    results = translator_malloc(i, sizeof(translator_result_t) * results_slots);
                    memcpy(results, results_stk, sizeof(translator_result_t) * translator_PARSE_STACK_MIN);
                } else if (j >= results_slots) {
                    results_slots = j + j / 2;
                    results = translator_realloc(i, results, sizeof(translator_result_t) * results_slots);
                }
            }

            *e = translator_err_merge(i, *e, results[j].error);
            translator_SUCCESS(
                    translator_parse_fold(i, p->data.repeat.f, j, (translator_val_t **) results);
                    if (j >= translator_PARSE_STACK_MIN) { translator_free(i, results); });

        case translator_TYPE_MANY1:

            results = results_stk;

            while (translator_parse_run(i, p->data.repeat.x, &results[j], e)) {
                j++;
                if (j == translator_PARSE_STACK_MIN) {
                    results_slots = j + j / 2;
                    results = translator_malloc(i, sizeof(translator_result_t) * results_slots);
                    memcpy(results, results_stk, sizeof(translator_result_t) * translator_PARSE_STACK_MIN);
                } else if (j >= results_slots) {
                    results_slots = j + j / 2;
                    results = translator_realloc(i, results, sizeof(translator_result_t) * results_slots);
                }
            }

            if (j == 0) {
                translator_FAILURE(
                        translator_err_many1(i, results[j].error);
                        if (j >= translator_PARSE_STACK_MIN) { translator_free(i, results); });
            } else {
                *e = translator_err_merge(i, *e, results[j].error);
                translator_SUCCESS(
                        translator_parse_fold(i, p->data.repeat.f, j, (translator_val_t **) results);
                        if (j >= translator_PARSE_STACK_MIN) { translator_free(i, results); });
            }

        case translator_TYPE_COUNT:

            results = p->data.repeat.n > translator_PARSE_STACK_MIN
                      ? translator_malloc(i, sizeof(translator_result_t) * p->data.repeat.n)
                      : results_stk;

            while (translator_parse_run(i, p->data.repeat.x, &results[j], e)) {
                j++;
                if (j == p->data.repeat.n) { break; }
            }

            if (j == p->data.repeat.n) {
                translator_SUCCESS(
                        translator_parse_fold(i, p->data.repeat.f, j, (translator_val_t **) results);
                        if (p->data.repeat.n > translator_PARSE_STACK_MIN) { translator_free(i, results); });
            } else {
                for (k = 0; k < j; k++) {
                    translator_parse_dtor(i, p->data.repeat.dx, results[k].output);
                }
                translator_FAILURE(
                        translator_err_count(i, results[j].error, p->data.repeat.n);
                        if (p->data.repeat.n > translator_PARSE_STACK_MIN) { translator_free(i, results); });
            }

            /* Combinatory Parsers */

        case translator_TYPE_OR:

            if (p->data.or.n == 0) { translator_SUCCESS(NULL); }

            results = p->data.or.n > translator_PARSE_STACK_MIN
                      ? translator_malloc(i, sizeof(translator_result_t) * p->data.or.n)
                      : results_stk;

            for (j = 0; j < p->data.or.n; j++) {
                if (translator_parse_run(i, p->data.or.xs[j], &results[j], e)) {
                    translator_SUCCESS(results[j].output;
                                               if (p->data.or.n > translator_PARSE_STACK_MIN) {
                                                   translator_free(i, results);
                                               });
                } else {
                    *e = translator_err_merge(i, *e, results[j].error);
                }
            }

            translator_FAILURE(NULL;
                                       if (p->data.or.n > translator_PARSE_STACK_MIN) { translator_free(i, results); });

        case translator_TYPE_AND:

            if (p->data.and.n == 0) { translator_SUCCESS(NULL); }

            results = p->data.or.n > translator_PARSE_STACK_MIN
                      ? translator_malloc(i, sizeof(translator_result_t) * p->data.or.n)
                      : results_stk;

            translator_input_mark(i);
            for (j = 0; j < p->data.and.n; j++) {
                if (!translator_parse_run(i, p->data.and.xs[j], &results[j], e)) {
                    translator_input_rewind(i);
                    for (k = 0; k < j; k++) {
                        translator_parse_dtor(i, p->data.and.dxs[k], results[k].output);
                    }
                    translator_FAILURE(results[j].error;
                                               if (p->data.or.n > translator_PARSE_STACK_MIN) {
                                                   translator_free(i, results);
                                               });
                }
            }
            translator_input_unmark(i);
            translator_SUCCESS(
                    translator_parse_fold(i, p->data.and.f, j, (translator_val_t **) results);
                    if (p->data.or.n > translator_PARSE_STACK_MIN) { translator_free(i, results); });

            /* End */

        default:

        translator_FAILURE(translator_err_fail(i, "Unknown Parser Type Id!"));
    }

    return 0;

}

#undef translator_SUCCESS
#undef translator_FAILURE
#undef translator_PRIMITIVE

int translator_parse_input(translator_input_t *i, translator_parser_t *p, translator_result_t *r) {
    int x;
    translator_err_t *e = translator_err_fail(i, "Unknown Error");
    e->state = translator_state_invalid();
    x = translator_parse_run(i, p, r, &e);
    if (x) {
        translator_err_delete_internal(i, e);
        r->output = translator_export(i, r->output);
    } else {
        r->error = translator_err_export(i, translator_err_merge(i, e, r->error));
    }
    return x;
}

int translator_parse(const char *filename, const char *string, translator_parser_t *p, translator_result_t *r) {
    int x;
    translator_input_t *i = translator_input_new_string(filename, string);
    x = translator_parse_input(i, p, r);
    translator_input_delete(i);
    return x;
}

int translator_nparse(const char *filename, const char *string, size_t length, translator_parser_t *p,
                      translator_result_t *r) {
    int x;
    translator_input_t *i = translator_input_new_nstring(filename, string, length);
    x = translator_parse_input(i, p, r);
    translator_input_delete(i);
    return x;
}

int translator_parse_file(const char *filename, FILE *file, translator_parser_t *p, translator_result_t *r) {
    int x;
    translator_input_t *i = translator_input_new_file(filename, file);
    x = translator_parse_input(i, p, r);
    translator_input_delete(i);
    return x;
}

int translator_parse_pipe(const char *filename, FILE *pipe, translator_parser_t *p, translator_result_t *r) {
    int x;
    translator_input_t *i = translator_input_new_pipe(filename, pipe);
    x = translator_parse_input(i, p, r);
    translator_input_delete(i);
    return x;
}

int translator_parse_contents(const char *filename, translator_parser_t *p, translator_result_t *r) {

    FILE *f = fopen(filename, "rb");
    int res;

    if (f == NULL) {
        r->output = NULL;
        r->error = translator_err_file(filename, "Unable to open file!");
        return 0;
    }

    res = translator_parse_file(filename, f, p, r);
    fclose(f);
    return res;
}

/*
** Building a Parser
*/

static void translator_undefine_unretained(translator_parser_t *p, int force);

static void translator_undefine_or(translator_parser_t *p) {

    int i;
    for (i = 0; i < p->data.or.n; i++) {
        translator_undefine_unretained(p->data.or.xs[i], 0);
    }
    free(p->data.or.xs);

}

static void translator_undefine_and(translator_parser_t *p) {

    int i;
    for (i = 0; i < p->data.and.n; i++) {
        translator_undefine_unretained(p->data.and.xs[i], 0);
    }
    free(p->data.and.xs);
    free(p->data.and.dxs);

}

static void translator_undefine_unretained(translator_parser_t *p, int force) {

    if (p->retained && !force) { return; }

    switch (p->type) {

        case translator_TYPE_FAIL:
            free(p->data.fail.m);
            break;

        case translator_TYPE_ONEOF:
        case translator_TYPE_NONEOF:
        case translator_TYPE_STRING:
            free(p->data.string.x);
            break;

        case translator_TYPE_APPLY:
            translator_undefine_unretained(p->data.apply.x, 0);
            break;
        case translator_TYPE_APPLY_TO:
            translator_undefine_unretained(p->data.apply_to.x, 0);
            break;
        case translator_TYPE_PREDICT:
            translator_undefine_unretained(p->data.predict.x, 0);
            break;

        case translator_TYPE_MAYBE:
        case translator_TYPE_NOT:
            translator_undefine_unretained(p->data.not.x, 0);
            break;

        case translator_TYPE_EXPECT:
            translator_undefine_unretained(p->data.expect.x, 0);
            free(p->data.expect.m);
            break;

        case translator_TYPE_MANY:
        case translator_TYPE_MANY1:
        case translator_TYPE_COUNT:
            translator_undefine_unretained(p->data.repeat.x, 0);
            break;

        case translator_TYPE_OR:
            translator_undefine_or(p);
            break;
        case translator_TYPE_AND:
            translator_undefine_and(p);
            break;

        default:
            break;
    }

    if (!force) {
        free(p->name);
        free(p);
    }

}

void translator_delete(translator_parser_t *p) {
    if (p->retained) {

        if (p->type != translator_TYPE_UNDEFINED) {
            translator_undefine_unretained(p, 0);
        }

        free(p->name);
        free(p);

    } else {
        translator_undefine_unretained(p, 0);
    }
}

static void translator_soft_delete(translator_val_t *x) {
    translator_undefine_unretained(x, 0);
}

static translator_parser_t *translator_undefined(void) {
    translator_parser_t *p = calloc(1, sizeof(translator_parser_t));
    p->retained = 0;
    p->type = translator_TYPE_UNDEFINED;
    p->name = NULL;
    return p;
}

translator_parser_t *translator_new(const char *name) {
    translator_parser_t *p = translator_undefined();
    p->retained = 1;
    p->name = realloc(p->name, strlen(name) + 1);
    strcpy(p->name, name);
    return p;
}

translator_parser_t *translator_copy(translator_parser_t *a) {
    int i = 0;
    translator_parser_t *p;

    if (a->retained) { return a; }

    p = translator_undefined();
    p->retained = a->retained;
    p->type = a->type;
    p->data = a->data;

    if (a->name) {
        p->name = malloc(strlen(a->name) + 1);
        strcpy(p->name, a->name);
    }

    switch (a->type) {

        case translator_TYPE_FAIL:
            p->data.fail.m = malloc(strlen(a->data.fail.m) + 1);
            strcpy(p->data.fail.m, a->data.fail.m);
            break;

        case translator_TYPE_ONEOF:
        case translator_TYPE_NONEOF:
        case translator_TYPE_STRING:
            p->data.string.x = malloc(strlen(a->data.string.x) + 1);
            strcpy(p->data.string.x, a->data.string.x);
            break;

        case translator_TYPE_APPLY:
            p->data.apply.x = translator_copy(a->data.apply.x);
            break;
        case translator_TYPE_APPLY_TO:
            p->data.apply_to.x = translator_copy(a->data.apply_to.x);
            break;
        case translator_TYPE_PREDICT:
            p->data.predict.x = translator_copy(a->data.predict.x);
            break;

        case translator_TYPE_MAYBE:
        case translator_TYPE_NOT:
            p->data.not.x = translator_copy(a->data.not.x);
            break;

        case translator_TYPE_EXPECT:
            p->data.expect.x = translator_copy(a->data.expect.x);
            p->data.expect.m = malloc(strlen(a->data.expect.m) + 1);
            strcpy(p->data.expect.m, a->data.expect.m);
            break;

        case translator_TYPE_MANY:
        case translator_TYPE_MANY1:
        case translator_TYPE_COUNT:
            p->data.repeat.x = translator_copy(a->data.repeat.x);
            break;

        case translator_TYPE_OR:
            p->data.or.xs = malloc(a->data.or.n * sizeof(translator_parser_t *));
            for (i = 0; i < a->data.or.n; i++) {
                p->data.or.xs[i] = translator_copy(a->data.or.xs[i]);
            }
            break;
        case translator_TYPE_AND:
            p->data.and.xs = malloc(a->data.and.n * sizeof(translator_parser_t *));
            for (i = 0; i < a->data.and.n; i++) {
                p->data.and.xs[i] = translator_copy(a->data.and.xs[i]);
            }
            p->data.and.dxs = malloc((a->data.and.n - 1) * sizeof(translator_dtor_t));
            for (i = 0; i < a->data.and.n - 1; i++) {
                p->data.and.dxs[i] = a->data.and.dxs[i];
            }
            break;

        default:
            break;
    }


    return p;
}

translator_parser_t *translator_undefine(translator_parser_t *p) {
    translator_undefine_unretained(p, 1);
    p->type = translator_TYPE_UNDEFINED;
    return p;
}

translator_parser_t *translator_define(translator_parser_t *p, translator_parser_t *a) {

    if (p->retained) {
        p->type = a->type;
        p->data = a->data;
    } else {
        translator_parser_t *a2 = translator_failf("Attempt to assign to Unretained Parser!");
        p->type = a2->type;
        p->data = a2->data;
        free(a2);
    }

    free(a);
    return p;
}

void translator_cleanup(int n, ...) {
    int i;
    translator_parser_t **list = malloc(sizeof(translator_parser_t *) * n);

    va_list va;
    va_start(va, n);
    for (i = 0; i < n; i++) { list[i] = va_arg(va, translator_parser_t*); }
    for (i = 0; i < n; i++) { translator_undefine(list[i]); }
    for (i = 0; i < n; i++) { translator_delete(list[i]); }
    va_end(va);

    free(list);
}

translator_parser_t *translator_pass(void) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_PASS;
    return p;
}

translator_parser_t *translator_fail(const char *m) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_FAIL;
    p->data.fail.m = malloc(strlen(m) + 1);
    strcpy(p->data.fail.m, m);
    return p;
}

/*
** As `snprintf` is not ANSI standard this
** function `translator_failf` should be considered
** unsafe.
**
** You have a few options if this is going to be
** trouble.
**
** - Ensure the format string does not exceed
**   the buffer length using precision specifiers
**   such as `%.512s`.
**
** - Patch this function in your code base to
**   use `snprintf` or whatever variant your
**   system supports.
**
** - Avoid it altogether.
**
*/

translator_parser_t *translator_failf(const char *fmt, ...) {

    va_list va;
    char *buffer;

    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_FAIL;

    va_start(va, fmt);
    buffer = malloc(2048);
    vsprintf(buffer, fmt, va);
    va_end(va);

    buffer = realloc(buffer, strlen(buffer) + 1);
    p->data.fail.m = buffer;
    return p;

}

translator_parser_t *translator_lift_val(translator_val_t *x) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_LIFT_VAL;
    p->data.lift.x = x;
    return p;
}

translator_parser_t *translator_lift(translator_ctor_t lf) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_LIFT;
    p->data.lift.lf = lf;
    return p;
}

translator_parser_t *translator_anchor(int(*f)(char, char)) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_ANCHOR;
    p->data.anchor.f = f;
    return translator_expect(p, "anchor");
}

translator_parser_t *translator_state(void) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_STATE;
    return p;
}

translator_parser_t *translator_expect(translator_parser_t *a, const char *expected) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_EXPECT;
    p->data.expect.x = a;
    p->data.expect.m = malloc(strlen(expected) + 1);
    strcpy(p->data.expect.m, expected);
    return p;
}

/*
** As `snprintf` is not ANSI standard this
** function `translator_expectf` should be considered
** unsafe.
**
** You have a few options if this is going to be
** trouble.
**
** - Ensure the format string does not exceed
**   the buffer length using precision specifiers
**   such as `%.512s`.
**
** - Patch this function in your code base to
**   use `snprintf` or whatever variant your
**   system supports.
**
** - Avoid it altogether.
**
*/

translator_parser_t *translator_expectf(translator_parser_t *a, const char *fmt, ...) {
    va_list va;
    char *buffer;

    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_EXPECT;

    va_start(va, fmt);
    buffer = malloc(2048);
    vsprintf(buffer, fmt, va);
    va_end(va);

    buffer = realloc(buffer, strlen(buffer) + 1);
    p->data.expect.x = a;
    p->data.expect.m = buffer;
    return p;
}

/*
** Basic Parsers
*/

translator_parser_t *translator_any(void) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_ANY;
    return translator_expect(p, "any character");
}

translator_parser_t *translator_char(char c) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_SINGLE;
    p->data.single.x = c;
    return translator_expectf(p, "'%c'", c);
}

translator_parser_t *translator_range(char s, char e) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_RANGE;
    p->data.range.x = s;
    p->data.range.y = e;
    return translator_expectf(p, "character between '%c' and '%c'", s, e);
}

translator_parser_t *translator_oneof(const char *s) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_ONEOF;
    p->data.string.x = malloc(strlen(s) + 1);
    strcpy(p->data.string.x, s);
    return translator_expectf(p, "one of '%s'", s);
}

translator_parser_t *translator_noneof(const char *s) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_NONEOF;
    p->data.string.x = malloc(strlen(s) + 1);
    strcpy(p->data.string.x, s);
    return translator_expectf(p, "none of '%s'", s);

}

translator_parser_t *translator_satisfy(int(*f)(char)) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_SATISFY;
    p->data.satisfy.f = f;
    return translator_expectf(p, "character satisfying function %p", f);
}

translator_parser_t *translator_string(const char *s) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_STRING;
    p->data.string.x = malloc(strlen(s) + 1);
    strcpy(p->data.string.x, s);
    return translator_expectf(p, "\"%s\"", s);
}

/*
** Core Parsers
*/

translator_parser_t *translator_apply(translator_parser_t *a, translator_apply_t f) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_APPLY;
    p->data.apply.x = a;
    p->data.apply.f = f;
    return p;
}

translator_parser_t *translator_apply_to(translator_parser_t *a, translator_apply_to_t f, void *x) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_APPLY_TO;
    p->data.apply_to.x = a;
    p->data.apply_to.f = f;
    p->data.apply_to.d = x;
    return p;
}

translator_parser_t *translator_predictive(translator_parser_t *a) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_PREDICT;
    p->data.predict.x = a;
    return p;
}

translator_parser_t *translator_not_lift(translator_parser_t *a, translator_dtor_t da, translator_ctor_t lf) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_NOT;
    p->data.not.x = a;
    p->data.not.dx = da;
    p->data.not.lf = lf;
    return p;
}

translator_parser_t *translator_not(translator_parser_t *a, translator_dtor_t da) {
    return translator_not_lift(a, da, translatorf_ctor_null);
}

translator_parser_t *translator_maybe_lift(translator_parser_t *a, translator_ctor_t lf) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_MAYBE;
    p->data.not.x = a;
    p->data.not.lf = lf;
    return p;
}

translator_parser_t *translator_maybe(translator_parser_t *a) {
    return translator_maybe_lift(a, translatorf_ctor_null);
}

translator_parser_t *translator_many(translator_fold_t f, translator_parser_t *a) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_MANY;
    p->data.repeat.x = a;
    p->data.repeat.f = f;
    return p;
}

translator_parser_t *translator_many1(translator_fold_t f, translator_parser_t *a) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_MANY1;
    p->data.repeat.x = a;
    p->data.repeat.f = f;
    return p;
}

translator_parser_t *translator_count(int n, translator_fold_t f, translator_parser_t *a, translator_dtor_t da) {
    translator_parser_t *p = translator_undefined();
    p->type = translator_TYPE_COUNT;
    p->data.repeat.n = n;
    p->data.repeat.f = f;
    p->data.repeat.x = a;
    p->data.repeat.dx = da;
    return p;
}

translator_parser_t *translator_or(int n, ...) {

    int i;
    va_list va;

    translator_parser_t *p = translator_undefined();

    p->type = translator_TYPE_OR;
    p->data.or.n = n;
    p->data.or.xs = malloc(sizeof(translator_parser_t *) * n);

    va_start(va, n);
    for (i = 0; i < n; i++) {
        p->data.or.xs[i] = va_arg(va, translator_parser_t*);
    }
    va_end(va);

    return p;
}

translator_parser_t *translator_and(int n, translator_fold_t f, ...) {

    int i;
    va_list va;

    translator_parser_t *p = translator_undefined();

    p->type = translator_TYPE_AND;
    p->data.and.n = n;
    p->data.and.f = f;
    p->data.and.xs = malloc(sizeof(translator_parser_t *) * n);
    p->data.and.dxs = malloc(sizeof(translator_dtor_t) * (n - 1));

    va_start(va, f);
    for (i = 0; i < n; i++) {
        p->data.and.xs[i] = va_arg(va, translator_parser_t*);
    }
    for (i = 0; i < (n - 1); i++) {
        p->data.and.dxs[i] = va_arg(va, translator_dtor_t);
    }
    va_end(va);

    return p;
}

/*
** Common Parsers
*/

static int translator_soi_anchor(char prev, char next) {
    (void) next;
    return (prev == '\0');
}

static int translator_eoi_anchor(char prev, char next) {
    (void) prev;
    return (next == '\0');
}

translator_parser_t *translator_soi(void) {
    return translator_expect(translator_anchor(translator_soi_anchor), "start of input");
}

translator_parser_t *translator_eoi(void) {
    return translator_expect(translator_anchor(translator_eoi_anchor), "end of input");
}

static int translator_boundary_anchor(char prev, char next) {
    const char *word = "abcdefghijklmnopqrstuvwxyz"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "0123456789_";
    if (strchr(word, next) && prev == '\0') { return 1; }
    if (strchr(word, prev) && next == '\0') { return 1; }
    if (strchr(word, next) && !strchr(word, prev)) { return 1; }
    if (!strchr(word, next) && strchr(word, prev)) { return 1; }
    return 0;
}

translator_parser_t *translator_boundary(void) {
    return translator_expect(translator_anchor(translator_boundary_anchor), "boundary");
}

translator_parser_t *translator_whitespace(void) {
    return translator_expect(translator_oneof(" \f\n\r\t\v"), "whitespace");
}

translator_parser_t *translator_whitespaces(void) {
    return translator_expect(translator_many(translatorf_strfold, translator_whitespace()), "spaces");
}

translator_parser_t *translator_blank(void) {
    return translator_expect(translator_apply(translator_whitespaces(), translatorf_free), "whitespace");
}

translator_parser_t *translator_newline(void) { return translator_expect(translator_char('\n'), "newline"); }

translator_parser_t *translator_tab(void) { return translator_expect(translator_char('\t'), "tab"); }

translator_parser_t *translator_escape(void) {
    return translator_and(2, translatorf_strfold, translator_char('\\'), translator_any(), free);
}

translator_parser_t *translator_digit(void) { return translator_expect(translator_oneof("0123456789"), "digit"); }

translator_parser_t *translator_hexdigit(void) {
    return translator_expect(translator_oneof("0123456789ABCDEFabcdef"), "hex digit");
}

translator_parser_t *translator_octdigit(void) { return translator_expect(translator_oneof("01234567"), "oct digit"); }

translator_parser_t *translator_digits(void) {
    return translator_expect(translator_many1(translatorf_strfold, translator_digit()), "digits");
}

translator_parser_t *translator_hexdigits(void) {
    return translator_expect(translator_many1(translatorf_strfold, translator_hexdigit()), "hex digits");
}

translator_parser_t *translator_octdigits(void) {
    return translator_expect(translator_many1(translatorf_strfold, translator_octdigit()), "oct digits");
}

translator_parser_t *translator_lower(void) {
    return translator_expect(translator_oneof("abcdefghijklmnopqrstuvwxyz"), "lowercase letter");
}

translator_parser_t *translator_upper(void) {
    return translator_expect(translator_oneof("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), "uppercase letter");
}

translator_parser_t *translator_alpha(void) {
    return translator_expect(translator_oneof("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), "letter");
}

translator_parser_t *translator_underscore(void) { return translator_expect(translator_char('_'), "underscore"); }

translator_parser_t *translator_alphanum(void) {
    return translator_expect(translator_or(3, translator_alpha(), translator_digit(), translator_underscore()),
                             "alphanumeric");
}

translator_parser_t *translator_int(void) {
    return translator_expect(translator_apply(translator_digits(), translatorf_int), "integer");
}

translator_parser_t *translator_hex(void) {
    return translator_expect(translator_apply(translator_hexdigits(), translatorf_hex), "hexadecimal");
}

translator_parser_t *translator_oct(void) {
    return translator_expect(translator_apply(translator_octdigits(), translatorf_oct), "octadecimal");
}

translator_parser_t *translator_number(void) {
    return translator_expect(translator_or(3, translator_int(), translator_hex(), translator_oct()), "number");
}

translator_parser_t *translator_real(void) {

    /* [+-]?\d+(\.\d+)?([eE][+-]?[0-9]+)? */

    translator_parser_t *p0, *p1, *p2, *p30, *p31, *p32, *p3;

    p0 = translator_maybe_lift(translator_oneof("+-"), translatorf_ctor_str);
    p1 = translator_digits();
    p2 = translator_maybe_lift(translator_and(2, translatorf_strfold, translator_char('.'), translator_digits(), free),
                               translatorf_ctor_str);
    p30 = translator_oneof("eE");
    p31 = translator_maybe_lift(translator_oneof("+-"), translatorf_ctor_str);
    p32 = translator_digits();
    p3 = translator_maybe_lift(translator_and(3, translatorf_strfold, p30, p31, p32, free, free), translatorf_ctor_str);

    return translator_expect(translator_and(4, translatorf_strfold, p0, p1, p2, p3, free, free, free), "real");

}

translator_parser_t *translator_float(void) {
    return translator_expect(translator_apply(translator_real(), translatorf_float), "float");
}

translator_parser_t *translator_char_lit(void) {
    return translator_expect(
            translator_between(translator_or(2, translator_escape(), translator_any()), free, "'", "'"), "char");
}

translator_parser_t *translator_string_lit(void) {
    translator_parser_t *strchar = translator_or(2, translator_escape(), translator_noneof("\""));
    return translator_expect(translator_between(translator_many(translatorf_strfold, strchar), free, "\"", "\""),
                             "string");
}

translator_parser_t *translator_regex_lit(void) {
    translator_parser_t *regexchar = translator_or(2, translator_escape(), translator_noneof("/"));
    return translator_expect(translator_between(translator_many(translatorf_strfold, regexchar), free, "/", "/"),
                             "regex");
}

translator_parser_t *translator_ident(void) {
    translator_parser_t *p0, *p1;
    p0 = translator_or(2, translator_alpha(), translator_underscore());
    p1 = translator_many(translatorf_strfold, translator_alphanum());
    return translator_and(2, translatorf_strfold, p0, p1, free);
}

/*
** Useful Parsers
*/

translator_parser_t *translator_startwith(translator_parser_t *a) {
    return translator_and(2, translatorf_snd, translator_soi(), a, translatorf_dtor_null);
}

translator_parser_t *translator_endwith(translator_parser_t *a, translator_dtor_t da) {
    return translator_and(2, translatorf_fst, a, translator_eoi(), da);
}

translator_parser_t *translator_whole(translator_parser_t *a, translator_dtor_t da) {
    return translator_and(3, translatorf_snd, translator_soi(), a, translator_eoi(), translatorf_dtor_null, da);
}

translator_parser_t *translator_stripl(translator_parser_t *a) {
    return translator_and(2, translatorf_snd, translator_blank(), a, translatorf_dtor_null);
}

translator_parser_t *translator_stripr(translator_parser_t *a) {
    return translator_and(2, translatorf_fst, a, translator_blank(), translatorf_dtor_null);
}

translator_parser_t *translator_strip(translator_parser_t *a) {
    return translator_and(3, translatorf_snd, translator_blank(), a, translator_blank(), translatorf_dtor_null,
                          translatorf_dtor_null);
}

translator_parser_t *translator_tok(translator_parser_t *a) {
    return translator_and(2, translatorf_fst, a, translator_blank(), translatorf_dtor_null);
}

translator_parser_t *translator_sym(const char *s) { return translator_tok(translator_string(s)); }

translator_parser_t *translator_total(translator_parser_t *a, translator_dtor_t da) {
    return translator_whole(translator_strip(a), da);
}

translator_parser_t *translator_between(translator_parser_t *a, translator_dtor_t ad, const char *o, const char *c) {
    return translator_and(3, translatorf_snd_free,
                          translator_string(o), a, translator_string(c),
                          free, ad);
}

translator_parser_t *translator_parens(translator_parser_t *a, translator_dtor_t ad) {
    return translator_between(a, ad, "(", ")");
}

translator_parser_t *translator_braces(translator_parser_t *a, translator_dtor_t ad) {
    return translator_between(a, ad, "<", ">");
}

translator_parser_t *translator_brackets(translator_parser_t *a, translator_dtor_t ad) {
    return translator_between(a, ad, "{", "}");
}

translator_parser_t *translator_squares(translator_parser_t *a, translator_dtor_t ad) {
    return translator_between(a, ad, "[", "]");
}

translator_parser_t *
translator_tok_between(translator_parser_t *a, translator_dtor_t ad, const char *o, const char *c) {
    return translator_and(3, translatorf_snd_free,
                          translator_sym(o), translator_tok(a), translator_sym(c),
                          free, ad);
}

translator_parser_t *
translator_tok_parens(translator_parser_t *a, translator_dtor_t ad) { return translator_tok_between(a, ad, "(", ")"); }

translator_parser_t *
translator_tok_braces(translator_parser_t *a, translator_dtor_t ad) { return translator_tok_between(a, ad, "<", ">"); }

translator_parser_t *translator_tok_brackets(translator_parser_t *a, translator_dtor_t ad) {
    return translator_tok_between(a, ad, "{", "}");
}

translator_parser_t *
translator_tok_squares(translator_parser_t *a, translator_dtor_t ad) { return translator_tok_between(a, ad, "[", "]"); }

/*
** Regular Expression Parsers
*/

/*
** So here is a cute bootstrapping.
**
** I'm using the previously defined
** translator constructs and functions to
** parse the user regex string and
** construct a parser from it.
**
** As it turns out lots of the standard
** translator functions look a lot like `fold`
** functions and so can be used indirectly
** by many of the parsing functions to build
** a parser directly - as we are parsing.
**
** This is certainly something that
** would be less elegant/interesting
** in a two-phase parser which first
** builds an AST and then traverses it
** to generate the object.
**
** This whole thing acts as a great
** case study for how trivial it can be
** to write a great parser in a few
** lines of code using translator.
*/

/*
**
**  ### Regular Expression Grammar
**
**      <regex> : <term> | (<term> "|" <regex>)
**
**      <term> : <factor>*
**
**      <factor> : <base>
**               | <base> "*"
**               | <base> "+"
**               | <base> "?"
**               | <base> "{" <digits> "}"
**
**      <base> : <char>
**             | "\" <char>
**             | "(" <regex> ")"
**             | "[" <range> "]"
*/

static translator_val_t *translatorf_re_or(int n, translator_val_t **xs) {
    (void) n;
    if (xs[1] == NULL) { return xs[0]; }
    else { return translator_or(2, xs[0], xs[1]); }
}

static translator_val_t *translatorf_re_and(int n, translator_val_t **xs) {
    int i;
    translator_parser_t *p = translator_lift(translatorf_ctor_str);
    for (i = 0; i < n; i++) {
        p = translator_and(2, translatorf_strfold, p, xs[i], free);
    }
    return p;
}

static translator_val_t *translatorf_re_repeat(int n, translator_val_t **xs) {
    int num;
    (void) n;
    if (xs[1] == NULL) { return xs[0]; }
    switch (((char *) xs[1])[0]) {
        case '*': {
            free(xs[1]);
            return translator_many(translatorf_strfold, xs[0]);
        };
            break;
        case '+': {
            free(xs[1]);
            return translator_many1(translatorf_strfold, xs[0]);
        };
            break;
        case '?': {
            free(xs[1]);
            return translator_maybe_lift(xs[0], translatorf_ctor_str);
        };
            break;
        default:
            num = *(int *) xs[1];
            free(xs[1]);
    }

    return translator_count(num, translatorf_strfold, xs[0], free);
}

static translator_parser_t *translator_re_escape_char(char c) {
    switch (c) {
        case 'a':
            return translator_char('\a');
        case 'f':
            return translator_char('\f');
        case 'n':
            return translator_char('\n');
        case 'r':
            return translator_char('\r');
        case 't':
            return translator_char('\t');
        case 'v':
            return translator_char('\v');
        case 'b':
            return translator_and(2, translatorf_snd, translator_boundary(), translator_lift(translatorf_ctor_str),
                                  free);
        case 'B':
            return translator_not_lift(translator_boundary(), free, translatorf_ctor_str);
        case 'A':
            return translator_and(2, translatorf_snd, translator_soi(), translator_lift(translatorf_ctor_str), free);
        case 'Z':
            return translator_and(2, translatorf_snd, translator_eoi(), translator_lift(translatorf_ctor_str), free);
        case 'd':
            return translator_digit();
        case 'D':
            return translator_not_lift(translator_digit(), free, translatorf_ctor_str);
        case 's':
            return translator_whitespace();
        case 'S':
            return translator_not_lift(translator_whitespace(), free, translatorf_ctor_str);
        case 'w':
            return translator_alphanum();
        case 'W':
            return translator_not_lift(translator_alphanum(), free, translatorf_ctor_str);
        default:
            return NULL;
    }
}

static translator_val_t *translatorf_re_escape(translator_val_t *x) {

    char *s = x;
    translator_parser_t *p;

    /* Regex Special Characters */
    if (s[0] == '.') {
        free(s);
        return translator_any();
    }
    if (s[0] == '^') {
        free(s);
        return translator_and(2, translatorf_snd, translator_soi(), translator_lift(translatorf_ctor_str), free);
    }
    if (s[0] == '$') {
        free(s);
        return translator_and(2, translatorf_snd, translator_eoi(), translator_lift(translatorf_ctor_str), free);
    }

    /* Regex Escape */
    if (s[0] == '\\') {
        p = translator_re_escape_char(s[1]);
        p = (p == NULL) ? translator_char(s[1]) : p;
        free(s);
        return p;
    }

    /* Regex Standard */
    p = translator_char(s[0]);
    free(s);
    return p;
}

static const char *translator_re_range_escape_char(char c) {
    switch (c) {
        case '-':
            return "-";
        case 'a':
            return "\a";
        case 'f':
            return "\f";
        case 'n':
            return "\n";
        case 'r':
            return "\r";
        case 't':
            return "\t";
        case 'v':
            return "\v";
        case 'b':
            return "\b";
        case 'd':
            return "0123456789";
        case 's':
            return " \f\n\r\t\v";
        case 'w':
            return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
        default:
            return NULL;
    }
}

static translator_val_t *translatorf_re_range(translator_val_t *x) {

    translator_parser_t *out;
    size_t i, j;
    size_t start, end;
    const char *tmp = NULL;
    const char *s = x;
    int comp = s[0] == '^' ? 1 : 0;
    char *range = calloc(1, 1);

    if (s[0] == '\0') {
        free(x);
        return translator_fail("Invalid Regex Range Expression");
    }
    if (s[0] == '^' &&
        s[1] == '\0') {
        free(x);
        return translator_fail("Invalid Regex Range Expression");
    }

    for (i = (size_t) comp; i < strlen(s); i++) {

        /* Regex Range Escape */
        if (s[i] == '\\') {
            tmp = translator_re_range_escape_char(s[i + 1]);
            if (tmp != NULL) {
                range = realloc(range, strlen(range) + strlen(tmp) + 1);
                strcat(range, tmp);
            } else {
                range = realloc(range, strlen(range) + 1 + 1);
                range[strlen(range) + 1] = '\0';
                range[strlen(range) + 0] = s[i + 1];
            }
            i++;
        }

            /* Regex Range...Range */
        else if (s[i] == '-') {
            if (s[i + 1] == '\0' || i == 0) {
                range = realloc(range, strlen(range) + strlen("-") + 1);
                strcat(range, "-");
            } else {
                start = (size_t) (s[i - 1] + 1);
                end = (size_t) (s[i + 1] - 1);
                for (j = start; j <= end; j++) {
                    range = realloc(range, strlen(range) + 1 + 1 + 1);
                    range[strlen(range) + 1] = '\0';
                    range[strlen(range) + 0] = (char) j;
                }
            }
        }

            /* Regex Range Normal */
        else {
            range = realloc(range, strlen(range) + 1 + 1);
            range[strlen(range) + 1] = '\0';
            range[strlen(range) + 0] = s[i];
        }

    }

    out = comp == 1 ? translator_noneof(range) : translator_oneof(range);

    free(x);
    free(range);

    return out;
}

translator_parser_t *translator_re(const char *re) {

    char *err_msg;
    translator_parser_t *err_out;
    translator_result_t r;
    translator_parser_t *Regex, *Term, *Factor, *Base, *Range, *RegexEnclose;

    Regex = translator_new("regex");
    Term = translator_new("term");
    Factor = translator_new("factor");
    Base = translator_new("base");
    Range = translator_new("range");

    translator_define(Regex, translator_and(2, translatorf_re_or,
                                            Term,
                                            translator_maybe(
                                                    translator_and(2, translatorf_snd_free, translator_char('|'), Regex,
                                                                   free)),
                                            (translator_dtor_t) translator_delete
    ));

    translator_define(Term, translator_many(translatorf_re_and, Factor));

    translator_define(Factor, translator_and(2, translatorf_re_repeat,
                                             Base,
                                             translator_or(5,
                                                           translator_char('*'), translator_char('+'),
                                                           translator_char('?'),
                                                           translator_brackets(translator_int(), free),
                                                           translator_pass()),
                                             (translator_dtor_t) translator_delete
    ));

    translator_define(Base, translator_or(4,
                                          translator_parens(Regex, (translator_dtor_t) translator_delete),
                                          translator_squares(Range, (translator_dtor_t) translator_delete),
                                          translator_apply(translator_escape(), translatorf_re_escape),
                                          translator_apply(translator_noneof(")|"), translatorf_re_escape)
    ));

    translator_define(Range, translator_apply(
            translator_many(translatorf_strfold, translator_or(2, translator_escape(), translator_noneof("]"))),
            translatorf_re_range
    ));

    RegexEnclose = translator_whole(translator_predictive(Regex), (translator_dtor_t) translator_delete);

    translator_optimise(RegexEnclose);
    translator_optimise(Regex);
    translator_optimise(Term);
    translator_optimise(Factor);
    translator_optimise(Base);
    translator_optimise(Range);

    if (!translator_parse("<translator_re_compiler>", re, RegexEnclose, &r)) {
        err_msg = translator_err_string(r.error);
        err_out = translator_failf("Invalid Regex: %s", err_msg);
        translator_err_delete(r.error);
        free(err_msg);
        r.output = err_out;
    }

    translator_cleanup(6, RegexEnclose, Regex, Term, Factor, Base, Range);

    translator_optimise(r.output);

    return r.output;

}

/*
** Common Fold Functions
*/

void translatorf_dtor_null(translator_val_t *x) {
    (void) x;
}

translator_val_t *translatorf_ctor_null(void) { return NULL; }

translator_val_t *translatorf_ctor_str(void) { return calloc(1, 1); }

translator_val_t *translatorf_free(translator_val_t *x) {
    free(x);
    return NULL;
}

translator_val_t *translatorf_int(translator_val_t *x) {
    int *y = malloc(sizeof(int));
    *y = (int) strtol(x, NULL, 10);
    free(x);
    return y;
}

translator_val_t *translatorf_hex(translator_val_t *x) {
    int *y = malloc(sizeof(int));
    *y = (int) strtol(x, NULL, 16);
    free(x);
    return y;
}

translator_val_t *translatorf_oct(translator_val_t *x) {
    int *y = malloc(sizeof(int));
    *y = (int) strtol(x, NULL, 8);
    free(x);
    return y;
}

translator_val_t *translatorf_float(translator_val_t *x) {
    float *y = malloc(sizeof(float));
    *y = (int) strtod(x, NULL);
    free(x);
    return y;
}

translator_val_t *translatorf_strtriml(translator_val_t *x) {
    char *s = x;
    while (isspace(*s)) {
        memmove(s, s + 1, strlen(s));
    }
    return s;
}

translator_val_t *translatorf_strtrimr(translator_val_t *x) {
    char *s = x;
    size_t l = strlen(s);
    while (isspace(s[l - 1])) {
        s[l - 1] = '\0';
        l--;
    }
    return s;
}

translator_val_t *translatorf_strtrim(translator_val_t *x) {
    return translatorf_strtriml(translatorf_strtrimr(x));
}

static const char translator_escape_input_c[] = {
        '\a', '\b', '\f', '\n', '\r',
        '\t', '\v', '\\', '\'', '\"', '\0'};

static const char *translator_escape_output_c[] = {
        "\\a", "\\b", "\\f", "\\n", "\\r", "\\t",
        "\\v", "\\\\", "\\'", "\\\"", "\\0", NULL};

static const char translator_escape_input_raw_re[] = {'/'};
static const char *translator_escape_output_raw_re[] = {"\\/", NULL};

static const char translator_escape_input_raw_cstr[] = {'"'};
static const char *translator_escape_output_raw_cstr[] = {"\\\"", NULL};

static const char translator_escape_input_raw_cchar[] = {'\''};
static const char *translator_escape_output_raw_cchar[] = {"\\'", NULL};

static translator_val_t *translatorf_escape_new(translator_val_t *x, const char *input, const char **output) {

    int i;
    int found;
    char buff[2];
    char *s = x;
    char *y = calloc(1, 1);

    while (*s) {

        i = 0;
        found = 0;

        while (output[i]) {
            if (*s == input[i]) {
                y = realloc(y, strlen(y) + strlen(output[i]) + 1);
                strcat(y, output[i]);
                found = 1;
                break;
            }
            i++;
        }

        if (!found) {
            y = realloc(y, strlen(y) + 2);
            buff[0] = *s;
            buff[1] = '\0';
            strcat(y, buff);
        }

        s++;
    }

    return y;
}

static translator_val_t *translatorf_unescape_new(translator_val_t *x, const char *input, const char **output) {

    int i;
    int found = 0;
    char buff[2];
    char *s = x;
    char *y = calloc(1, 1);

    while (*s) {

        i = 0;
        found = 0;

        while (output[i]) {
            if ((*(s + 0)) == output[i][0] &&
                (*(s + 1)) == output[i][1]) {
                y = realloc(y, strlen(y) + 1 + 1);
                buff[0] = input[i];
                buff[1] = '\0';
                strcat(y, buff);
                found = 1;
                s++;
                break;
            }
            i++;
        }

        if (!found) {
            y = realloc(y, strlen(y) + 1 + 1);
            buff[0] = *s;
            buff[1] = '\0';
            strcat(y, buff);
        }

        if (*s == '\0') { break; }
        else { s++; }
    }

    return y;

}

translator_val_t *translatorf_escape(translator_val_t *x) {
    translator_val_t *y = translatorf_escape_new(x, translator_escape_input_c, translator_escape_output_c);
    free(x);
    return y;
}

translator_val_t *translatorf_unescape(translator_val_t *x) {
    translator_val_t *y = translatorf_unescape_new(x, translator_escape_input_c, translator_escape_output_c);
    free(x);
    return y;
}

translator_val_t *translatorf_escape_regex(translator_val_t *x) {
    translator_val_t *y = translatorf_escape_new(x, translator_escape_input_raw_re, translator_escape_output_raw_re);
    free(x);
    return y;
}

translator_val_t *translatorf_unescape_regex(translator_val_t *x) {
    translator_val_t *y = translatorf_unescape_new(x, translator_escape_input_raw_re, translator_escape_output_raw_re);
    free(x);
    return y;
}

translator_val_t *translatorf_escape_string_raw(translator_val_t *x) {
    translator_val_t *y = translatorf_escape_new(x, translator_escape_input_raw_cstr,
                                                 translator_escape_output_raw_cstr);
    free(x);
    return y;
}

translator_val_t *translatorf_unescape_string_raw(translator_val_t *x) {
    translator_val_t *y = translatorf_unescape_new(x, translator_escape_input_raw_cstr,
                                                   translator_escape_output_raw_cstr);
    free(x);
    return y;
}

translator_val_t *translatorf_escape_char_raw(translator_val_t *x) {
    translator_val_t *y = translatorf_escape_new(x, translator_escape_input_raw_cchar,
                                                 translator_escape_output_raw_cchar);
    free(x);
    return y;
}

translator_val_t *translatorf_unescape_char_raw(translator_val_t *x) {
    translator_val_t *y = translatorf_unescape_new(x, translator_escape_input_raw_cchar,
                                                   translator_escape_output_raw_cchar);
    free(x);
    return y;
}

translator_val_t *translatorf_null(int n, translator_val_t **xs) {
    (void) n;
    (void) xs;
    return NULL;
}

translator_val_t *translatorf_fst(int n, translator_val_t **xs) {
    (void) n;
    return xs[0];
}

translator_val_t *translatorf_snd(int n, translator_val_t **xs) {
    (void) n;
    return xs[1];
}

translator_val_t *translatorf_trd(int n, translator_val_t **xs) {
    (void) n;
    return xs[2];
}

static translator_val_t *translatorf_nth_free(int n, translator_val_t **xs, int x) {
    int i;
    for (i = 0; i < n; i++) {
        if (i != x) { free(xs[i]); }
    }
    return xs[x];
}

translator_val_t *translatorf_fst_free(int n, translator_val_t **xs) { return translatorf_nth_free(n, xs, 0); }

translator_val_t *translatorf_snd_free(int n, translator_val_t **xs) { return translatorf_nth_free(n, xs, 1); }

translator_val_t *translatorf_trd_free(int n, translator_val_t **xs) { return translatorf_nth_free(n, xs, 2); }

translator_val_t *translatorf_strfold(int n, translator_val_t **xs) {
    int i;
    size_t l = 0;

    if (n == 0) { return calloc(1, 1); }

    for (i = 0; i < n; i++) { l += strlen(xs[i]); }

    xs[0] = realloc(xs[0], l + 1);

    for (i = 1; i < n; i++) {
        strcat(xs[0], xs[i]);
        free(xs[i]);
    }

    return xs[0];
}

translator_val_t *translatorf_maths(int n, translator_val_t **xs) {
    int **vs = (int **) xs;
    (void) n;

    switch (((char *) xs[1])[0]) {
        case '*': {
            *vs[0] *= *vs[2];
        };
            break;
        case '/': {
            *vs[0] /= *vs[2];
        };
            break;
        case '%': {
            *vs[0] %= *vs[2];
        };
            break;
        case '+': {
            *vs[0] += *vs[2];
        };
            break;
        case '-': {
            *vs[0] -= *vs[2];
        };
            break;
        default:
            break;
    }

    free(xs[1]);
    free(xs[2]);

    return xs[0];
}

/*
** Printing
*/

static void translator_print_unretained(translator_parser_t *p, int force) {

    /* TODO: Print Everything Escaped */

    int i;
    char *s, *e;
    char buff[2];

    if (p->retained && !force) { ;
        if (p->name) { printf("<%s>", p->name); }
        else { printf("<anon>"); }
        return;
    }

    if (p->type == translator_TYPE_UNDEFINED) { printf("<?>"); }
    if (p->type == translator_TYPE_PASS) { printf("<:>"); }
    if (p->type == translator_TYPE_FAIL) { printf("<!>"); }
    if (p->type == translator_TYPE_LIFT) { printf("<#>"); }
    if (p->type == translator_TYPE_STATE) { printf("<S>"); }
    if (p->type == translator_TYPE_ANCHOR) { printf("<@>"); }
    if (p->type == translator_TYPE_EXPECT) {
        printf("%s", p->data.expect.m);
        /*translator_print_unretained(p->data.expect.x, 0);*/
    }

    if (p->type == translator_TYPE_ANY) { printf("<.>"); }
    if (p->type == translator_TYPE_SATISFY) { printf("<f>"); }

    if (p->type == translator_TYPE_SINGLE) {
        buff[0] = p->data.single.x;
        buff[1] = '\0';
        s = translatorf_escape_new(
                buff,
                translator_escape_input_c,
                translator_escape_output_c);
        printf("'%s'", s);
        free(s);
    }

    if (p->type == translator_TYPE_RANGE) {
        buff[0] = p->data.range.x;
        buff[1] = '\0';
        s = translatorf_escape_new(
                buff,
                translator_escape_input_c,
                translator_escape_output_c);
        buff[0] = p->data.range.y;
        buff[1] = '\0';
        e = translatorf_escape_new(
                buff,
                translator_escape_input_c,
                translator_escape_output_c);
        printf("[%s-%s]", s, e);
        free(s);
        free(e);
    }

    if (p->type == translator_TYPE_ONEOF) {
        s = translatorf_escape_new(
                p->data.string.x,
                translator_escape_input_c,
                translator_escape_output_c);
        printf("[%s]", s);
        free(s);
    }

    if (p->type == translator_TYPE_NONEOF) {
        s = translatorf_escape_new(
                p->data.string.x,
                translator_escape_input_c,
                translator_escape_output_c);
        printf("[^%s]", s);
        free(s);
    }

    if (p->type == translator_TYPE_STRING) {
        s = translatorf_escape_new(
                p->data.string.x,
                translator_escape_input_c,
                translator_escape_output_c);
        printf("\"%s\"", s);
        free(s);
    }

    if (p->type == translator_TYPE_APPLY) { translator_print_unretained(p->data.apply.x, 0); }
    if (p->type == translator_TYPE_APPLY_TO) { translator_print_unretained(p->data.apply_to.x, 0); }
    if (p->type == translator_TYPE_PREDICT) { translator_print_unretained(p->data.predict.x, 0); }

    if (p->type == translator_TYPE_NOT) {
        translator_print_unretained(p->data.not.x, 0);
        printf("!");
    }
    if (p->type == translator_TYPE_MAYBE) {
        translator_print_unretained(p->data.not.x, 0);
        printf("?");
    }

    if (p->type == translator_TYPE_MANY) {
        translator_print_unretained(p->data.repeat.x, 0);
        printf("*");
    }
    if (p->type == translator_TYPE_MANY1) {
        translator_print_unretained(p->data.repeat.x, 0);
        printf("+");
    }
    if (p->type == translator_TYPE_COUNT) {
        translator_print_unretained(p->data.repeat.x, 0);
        printf("{%i}", p->data.repeat.n);
    }

    if (p->type == translator_TYPE_OR) {
        printf("(");
        for (i = 0; i < p->data.or.n - 1; i++) {
            translator_print_unretained(p->data.or.xs[i], 0);
            printf(" | ");
        }
        translator_print_unretained(p->data.or.xs[p->data.or.n - 1], 0);
        printf(")");
    }

    if (p->type == translator_TYPE_AND) {
        printf("(");
        for (i = 0; i < p->data.and.n - 1; i++) {
            translator_print_unretained(p->data.and.xs[i], 0);
            printf(" ");
        }
        translator_print_unretained(p->data.and.xs[p->data.and.n - 1], 0);
        printf(")");
    }

}

void translator_print(translator_parser_t *p) {
    translator_print_unretained(p, 1);
    printf("\n");
}

/*
** Testing
*/

/*
** These functions are slightly unwieldy and
** also the whole of the testing suite for translator
** translator is pretty shaky.
**
** It could do with a lot more tests and more
** precision. Currently I am only really testing
** changes off of the examples.
**
*/

int translator_test_fail(translator_parser_t *p, const char *s, const void *d,
                         int(*tester)(const void *, const void *),
                         translator_dtor_t destructor,
                         void(*printer)(const void *)) {
    translator_result_t r;
    (void) printer;
    if (translator_parse("<test>", s, p, &r)) {

        if (tester(r.output, d)) {
            destructor(r.output);
            return 0;
        } else {
            destructor(r.output);
            return 1;
        }

    } else {
        translator_err_delete(r.error);
        return 1;
    }

}

int translator_test_pass(translator_parser_t *p, const char *s, const void *d,
                         int(*tester)(const void *, const void *),
                         translator_dtor_t destructor,
                         void(*printer)(const void *)) {

    translator_result_t r;
    if (translator_parse("<test>", s, p, &r)) {

        if (tester(r.output, d)) {
            destructor(r.output);
            return 1;
        } else {
            printf("Got ");
            printer(r.output);
            printf("\n");
            printf("Expected ");
            printer(d);
            printf("\n");
            destructor(r.output);
            return 0;
        }

    } else {
        translator_err_print(r.error);
        translator_err_delete(r.error);
        return 0;

    }

}


/*
** AST
*/

void translator_ast_delete(translator_ast_t *a) {

    int i;

    if (a == NULL) { return; }

    for (i = 0; i < a->children_num; i++) {
        translator_ast_delete(a->children[i]);
    }

    free(a->children);
    free(a->tag);
    free(a->contents);
    free(a);

}

static void translator_ast_delete_no_children(translator_ast_t *a) {
    free(a->children);
    free(a->tag);
    free(a->contents);
    free(a);
}

translator_ast_t *translator_ast_new(const char *tag, const char *contents) {

    translator_ast_t *a = malloc(sizeof(translator_ast_t));

    a->tag = malloc(strlen(tag) + 1);
    strcpy(a->tag, tag);

    a->contents = malloc(strlen(contents) + 1);
    strcpy(a->contents, contents);

    a->state = translator_state_new();

    a->children_num = 0;
    a->children = NULL;

    return a;

}

translator_ast_t *translator_ast_build(int n, const char *tag, ...) {

    translator_ast_t *a = translator_ast_new(tag, "");

    int i;
    va_list va;
    va_start(va, tag);

    for (i = 0; i < n; i++) {
        translator_ast_add_child(a, va_arg(va, translator_ast_t*));
    }

    va_end(va);

    return a;
}

translator_ast_t *translator_ast_add_root(translator_ast_t *a) {

    translator_ast_t *r;

    if (a == NULL) { return a; }
    if (a->children_num == 0) { return a; }
    if (a->children_num == 1) { return a; }

    r = translator_ast_new(">", "");
    translator_ast_add_child(r, a);
    return r;
}

int translator_ast_eq(translator_ast_t *a, translator_ast_t *b) {

    int i;

    if (strcmp(a->tag, b->tag) != 0) { return 0; }
    if (strcmp(a->contents, b->contents) != 0) { return 0; }
    if (a->children_num != b->children_num) { return 0; }

    for (i = 0; i < a->children_num; i++) {
        if (!translator_ast_eq(a->children[i], b->children[i])) { return 0; }
    }

    return 1;
}

translator_ast_t *translator_ast_add_child(translator_ast_t *r, translator_ast_t *a) {
    r->children_num++;
    r->children = realloc(r->children, sizeof(translator_ast_t *) * r->children_num);
    r->children[r->children_num - 1] = a;
    return r;
}

translator_ast_t *translator_ast_add_tag(translator_ast_t *a, const char *t) {
    if (a == NULL) { return a; }
    a->tag = realloc(a->tag, strlen(t) + 1 + strlen(a->tag) + 1);
    memmove(a->tag + strlen(t) + 1, a->tag, strlen(a->tag) + 1);
    memmove(a->tag, t, strlen(t));
    memmove(a->tag + strlen(t), "|", 1);
    return a;
}

translator_ast_t *translator_ast_add_root_tag(translator_ast_t *a, const char *t) {
    if (a == NULL) { return a; }
    a->tag = realloc(a->tag, (strlen(t) - 1) + strlen(a->tag) + 1);
    memmove(a->tag + (strlen(t) - 1), a->tag, strlen(a->tag) + 1);
    memmove(a->tag, t, (strlen(t) - 1));
    return a;
}

translator_ast_t *translator_ast_tag(translator_ast_t *a, const char *t) {
    a->tag = realloc(a->tag, strlen(t) + 1);
    strcpy(a->tag, t);
    return a;
}

translator_ast_t *translator_ast_state(translator_ast_t *a, translator_state_t s) {
    if (a == NULL) { return a; }
    a->state = s;
    return a;
}

static void translator_ast_print_depth(translator_ast_t *a, int d, FILE *fp) {

    int i;

    if (a == NULL) {
        fprintf(fp, "NULL\n");
        return;
    }

    for (i = 0; i < d; i++) { fprintf(fp, "  "); }

    if (strlen(a->contents)) {
        fprintf(fp, "%s:%lu:%lu '%s'\n", a->tag,
                (long unsigned int) (a->state.row + 1),
                (long unsigned int) (a->state.col + 1),
                a->contents);
    } else {
        fprintf(fp, "%s \n", a->tag);
    }

    for (i = 0; i < a->children_num; i++) {
        translator_ast_print_depth(a->children[i], d + 1, fp);
    }

}

void translator_ast_print(translator_ast_t *a) {
    translator_ast_print_depth(a, 0, stdout);
}

void translator_ast_print_to(translator_ast_t *a, FILE *fp) {
    translator_ast_print_depth(a, 0, fp);
}

int translator_ast_get_index(translator_ast_t *ast, const char *tag) {
    return translator_ast_get_index_lb(ast, tag, 0);
}

int translator_ast_get_index_lb(translator_ast_t *ast, const char *tag, int lb) {
    int i;

    for (i = lb; i < ast->children_num; i++) {
        if (strcmp(ast->children[i]->tag, tag) == 0) {
            return i;
        }
    }

    return -1;
}

translator_ast_t *translator_ast_get_child(translator_ast_t *ast, const char *tag) {
    return translator_ast_get_child_lb(ast, tag, 0);
}

translator_ast_t *translator_ast_get_child_lb(translator_ast_t *ast, const char *tag, int lb) {
    int i;

    for (i = lb; i < ast->children_num; i++) {
        if (strcmp(ast->children[i]->tag, tag) == 0) {
            return ast->children[i];
        }
    }

    return NULL;
}

translator_ast_trav_t *translator_ast_traverse_start(translator_ast_t *ast,
                                                     translator_ast_trav_order_t order) {
    translator_ast_trav_t *trav, *n_trav;
    translator_ast_t *cnode = ast;

    /* Create the traversal structure */
    trav = malloc(sizeof(translator_ast_trav_t));
    trav->curr_node = cnode;
    trav->parent = NULL;
    trav->curr_child = 0;
    trav->order = order;

    /* Get start node */
    switch (order) {
        case translator_ast_trav_order_pre:
            /* Nothing else is needed for pre order start */
            break;

        case translator_ast_trav_order_post:
            while (cnode->children_num > 0) {
                cnode = cnode->children[0];

                n_trav = malloc(sizeof(translator_ast_trav_t));
                n_trav->curr_node = cnode;
                n_trav->parent = trav;
                n_trav->curr_child = 0;
                n_trav->order = order;

                trav = n_trav;
            }

            break;

        default:
            /* Unreachable, but compiler complaints */
            break;
    }

    return trav;
}

translator_ast_t *translator_ast_traverse_next(translator_ast_trav_t **trav) {
    translator_ast_trav_t *n_trav, *to_free;
    translator_ast_t *ret = NULL;
    int cchild;

    /* The end of traversal was reached */
    if (*trav == NULL) return NULL;

    switch ((*trav)->order) {
        case translator_ast_trav_order_pre:
            ret = (*trav)->curr_node;

            /* If there aren't any more children, go up */
            while (*trav != NULL &&
                   (*trav)->curr_child >= (*trav)->curr_node->children_num) {
                to_free = *trav;
                *trav = (*trav)->parent;
                free(to_free);
            }

            /* If trav is NULL, the end was reached */
            if (*trav == NULL) {
                break;
            }

            /* Go to next child */
            n_trav = malloc(sizeof(translator_ast_trav_t));

            cchild = (*trav)->curr_child;
            n_trav->curr_node = (*trav)->curr_node->children[cchild];
            n_trav->parent = *trav;
            n_trav->curr_child = 0;
            n_trav->order = (*trav)->order;

            (*trav)->curr_child++;
            *trav = n_trav;

            break;

        case translator_ast_trav_order_post:
            ret = (*trav)->curr_node;

            /* Move up tree to the parent If the parent doesn't have any more nodes,
             * then this is the current node. If it does, move down to its left most
             * child. Also, free the previous traversal node */
            to_free = *trav;
            *trav = (*trav)->parent;
            free(to_free);

            if (*trav == NULL)
                break;

            /* Next child */
            (*trav)->curr_child++;

            /* If there aren't any more children, this is the next node */
            if ((*trav)->curr_child >= (*trav)->curr_node->children_num) {
                break;
            }

            /* If there are still more children, find the leftmost child from this
             * node */
            while ((*trav)->curr_node->children_num > 0) {
                n_trav = malloc(sizeof(translator_ast_trav_t));

                cchild = (*trav)->curr_child;
                n_trav->curr_node = (*trav)->curr_node->children[cchild];
                n_trav->parent = *trav;
                n_trav->curr_child = 0;
                n_trav->order = (*trav)->order;

                *trav = n_trav;
            }

        default:
            /* Unreachable, but compiler complaints */
            break;
    }

    return ret;
}

void translator_ast_traverse_free(translator_ast_trav_t **trav) {
    translator_ast_trav_t *n_trav;

    /* Go through parents until all are free */
    while (*trav != NULL) {
        n_trav = (*trav)->parent;
        free(*trav);
        *trav = n_trav;
    }
}

translator_val_t *translatorf_fold_ast(int n, translator_val_t **xs) {

    int i, j;
    translator_ast_t **as = (translator_ast_t **) xs;
    translator_ast_t *r;

    if (n == 0) { return NULL; }
    if (n == 1) { return xs[0]; }
    if (n == 2 && xs[1] == NULL) { return xs[0]; }
    if (n == 2 && xs[0] == NULL) { return xs[1]; }

    r = translator_ast_new(">", "");

    for (i = 0; i < n; i++) {

        if (as[i] == NULL) { continue; }

        if (as[i] && as[i]->children_num == 0) {
            translator_ast_add_child(r, as[i]);
        } else if (as[i] && as[i]->children_num == 1) {
            translator_ast_add_child(r, translator_ast_add_root_tag(as[i]->children[0], as[i]->tag));
            translator_ast_delete_no_children(as[i]);
        } else if (as[i] && as[i]->children_num >= 2) {
            for (j = 0; j < as[i]->children_num; j++) {
                translator_ast_add_child(r, as[i]->children[j]);
            }
            translator_ast_delete_no_children(as[i]);
        }

    }

    if (r->children_num) {
        r->state = r->children[0]->state;
    }

    return r;
}

translator_val_t *translatorf_str_ast(translator_val_t *c) {
    translator_ast_t *a = translator_ast_new("", c);
    free(c);
    return a;
}

translator_val_t *translatorf_state_ast(int n, translator_val_t **xs) {
    translator_state_t *s = ((translator_state_t **) xs)[0];
    translator_ast_t *a = ((translator_ast_t **) xs)[1];
    (void) n;
    a = translator_ast_state(a, *s);
    free(s);
    return a;
}

translator_parser_t *translatora_state(translator_parser_t *a) {
    return translator_and(2, translatorf_state_ast, translator_state(), a, free);
}

translator_parser_t *translatora_tag(translator_parser_t *a, const char *t) {
    return translator_apply_to(a, (translator_apply_to_t) translator_ast_tag, (void *) t);
}

translator_parser_t *translatora_add_tag(translator_parser_t *a, const char *t) {
    return translator_apply_to(a, (translator_apply_to_t) translator_ast_add_tag, (void *) t);
}

translator_parser_t *translatora_root(translator_parser_t *a) {
    return translator_apply(a, (translator_apply_t) translator_ast_add_root);
}

translator_parser_t *translatora_not(translator_parser_t *a) {
    return translator_not(a, (translator_dtor_t) translator_ast_delete);
}

translator_parser_t *translatora_maybe(translator_parser_t *a) { return translator_maybe(a); }

translator_parser_t *translatora_many(translator_parser_t *a) { return translator_many(translatorf_fold_ast, a); }

translator_parser_t *translatora_many1(translator_parser_t *a) { return translator_many1(translatorf_fold_ast, a); }

translator_parser_t *translatora_count(int n, translator_parser_t *a) {
    return translator_count(n, translatorf_fold_ast, a, (translator_dtor_t) translator_ast_delete);
}

translator_parser_t *translatora_or(int n, ...) {

    int i;
    va_list va;

    translator_parser_t *p = translator_undefined();

    p->type = translator_TYPE_OR;
    p->data.or.n = n;
    p->data.or.xs = malloc(sizeof(translator_parser_t *) * n);

    va_start(va, n);
    for (i = 0; i < n; i++) {
        p->data.or.xs[i] = va_arg(va, translator_parser_t*);
    }
    va_end(va);

    return p;

}

translator_parser_t *translatora_and(int n, ...) {

    int i;
    va_list va;

    translator_parser_t *p = translator_undefined();

    p->type = translator_TYPE_AND;
    p->data.and.n = n;
    p->data.and.f = translatorf_fold_ast;
    p->data.and.xs = malloc(sizeof(translator_parser_t *) * n);
    p->data.and.dxs = malloc(sizeof(translator_dtor_t) * (n - 1));

    va_start(va, n);
    for (i = 0; i < n; i++) {
        p->data.and.xs[i] = va_arg(va, translator_parser_t*);
    }
    for (i = 0; i < (n - 1); i++) {
        p->data.and.dxs[i] = (translator_dtor_t) translator_ast_delete;
    }
    va_end(va);

    return p;
}

translator_parser_t *translatora_total(translator_parser_t *a) {
    return translator_total(a, (translator_dtor_t) translator_ast_delete);
}

/*
** Grammar Parser
*/

/*
** This is another interesting bootstrapping.
**
** Having a general purpose AST type allows
** users to specify the grammar alone and
** let all fold rules be automatically taken
** care of by existing functions.
**
** You don't get to control the type spat
** out but this means you can make a nice
** parser to take in some grammar in nice
** syntax and spit out a parser that works.
**
** The grammar for this looks surprisingly
** like regex but the main difference is that
** it is now whitespace insensitive and the
** base type takes literals of some form.
*/

/*
**
**  ### Grammar Grammar
**
**      <grammar> : (<term> "|" <grammar>) | <term>
**
**      <term> : <factor>*
**
**      <factor> : <base>
**               | <base> "*"
**               | <base> "+"
**               | <base> "?"
**               | <base> "{" <digits> "}"
**
**      <base> : "<" (<digits> | <ident>) ">"
**             | <string_lit>
**             | <char_lit>
**             | <regex_lit>
**             | "(" <grammar> ")"
*/

typedef struct {
    va_list *va;
    int parsers_num;
    translator_parser_t **parsers;
    int flags;
} translatora_grammar_st_t;

static translator_val_t *translatoraf_grammar_or(int n, translator_val_t **xs) {
    (void) n;
    if (xs[1] == NULL) { return xs[0]; }
    else { return translatora_or(2, xs[0], xs[1]); }
}

static translator_val_t *translatoraf_grammar_and(int n, translator_val_t **xs) {
    int i;
    translator_parser_t *p = translator_pass();
    for (i = 0; i < n; i++) {
        if (xs[i] != NULL) { p = translatora_and(2, p, xs[i]); }
    }
    return p;
}

static translator_val_t *translatoraf_grammar_repeat(int n, translator_val_t **xs) {
    int num;
    (void) n;
    if (xs[1] == NULL) { return xs[0]; }
    switch (((char *) xs[1])[0]) {
        case '*': {
            free(xs[1]);
            return translatora_many(xs[0]);
        };
            break;
        case '+': {
            free(xs[1]);
            return translatora_many1(xs[0]);
        };
            break;
        case '?': {
            free(xs[1]);
            return translatora_maybe(xs[0]);
        };
            break;
        case '!': {
            free(xs[1]);
            return translatora_not(xs[0]);
        };
            break;
        default:
            num = *((int *) xs[1]);
            free(xs[1]);
    }
    return translatora_count(num, xs[0]);
}

static translator_val_t *translatoraf_grammar_string(translator_val_t *x, void *s) {
    translatora_grammar_st_t *st = s;
    char *y = translatorf_unescape(x);
    translator_parser_t *p = (st->flags & TRANSLATOR_LANG_WHITESPACE_SENSITIVE) ? translator_string(y) : translator_tok(
            translator_string(y));
    free(y);
    return translatora_state(translatora_tag(translator_apply(p, translatorf_str_ast), "string"));
}

static translator_val_t *translatoraf_grammar_char(translator_val_t *x, void *s) {
    translatora_grammar_st_t *st = s;
    char *y = translatorf_unescape(x);
    translator_parser_t *p = (st->flags & TRANSLATOR_LANG_WHITESPACE_SENSITIVE) ? translator_char(y[0])
                                                                                : translator_tok(translator_char(y[0]));
    free(y);
    return translatora_state(translatora_tag(translator_apply(p, translatorf_str_ast), "char"));
}

static translator_val_t *translatoraf_grammar_regex(translator_val_t *x, void *s) {
    translatora_grammar_st_t *st = s;
    char *y = translatorf_unescape_regex(x);
    translator_parser_t *p = (st->flags & TRANSLATOR_LANG_WHITESPACE_SENSITIVE) ? translator_re(y) : translator_tok(
            translator_re(y));
    free(y);
    return translatora_state(translatora_tag(translator_apply(p, translatorf_str_ast), "regex"));
}

/* Should this just use `isdigit` instead? */
static int is_number(const char *s) {
    size_t i;
    for (i = 0; i < strlen(s); i++) { if (!strchr("0123456789", s[i])) { return 0; }}
    return 1;
}

static translator_parser_t *translatora_grammar_find_parser(char *x, translatora_grammar_st_t *st) {

    int i;
    translator_parser_t *p;

    /* Case of Number */
    if (is_number(x)) {

        i = (int) strtol(x, NULL, 10);

        while (st->parsers_num <= i) {
            st->parsers_num++;
            st->parsers = realloc(st->parsers, sizeof(translator_parser_t *) * st->parsers_num);
            st->parsers[st->parsers_num - 1] = va_arg(*st->va, translator_parser_t*);
            if (st->parsers[st->parsers_num - 1] == NULL) {
                return translator_failf("No Parser in position %i! Only supplied %i Parsers!", i, st->parsers_num);
            }
        }

        return st->parsers[st->parsers_num - 1];

        /* Case of Identifier */
    } else {

        /* Search Existing Parsers */
        for (i = 0; i < st->parsers_num; i++) {
            translator_parser_t *q = st->parsers[i];
            if (q == NULL) { return translator_failf("Unknown Parser '%s'!", x); }
            if (q->name && strcmp(q->name, x) == 0) { return q; }
        }

        /* Search New Parsers */
        while (1) {

            p = va_arg(*st->va, translator_parser_t*);

            st->parsers_num++;
            st->parsers = realloc(st->parsers, sizeof(translator_parser_t *) * st->parsers_num);
            st->parsers[st->parsers_num - 1] = p;

            if (p == NULL || p->name == NULL) { return translator_failf("Unknown Parser '%s'!", x); }
            if (p->name && strcmp(p->name, x) == 0) { return p; }

        }

    }

}

static translator_val_t *translatoraf_grammar_id(translator_val_t *x, void *s) {

    translatora_grammar_st_t *st = s;
    translator_parser_t *p = translatora_grammar_find_parser(x, st);
    free(x);

    if (p->name) {
        return translatora_state(translatora_root(translatora_add_tag(p, p->name)));
    } else {
        return translatora_state(translatora_root(p));
    }
}

translator_parser_t *translatora_grammar_st(const char *grammar, translatora_grammar_st_t *st) {

    char *err_msg;
    translator_parser_t *err_out;
    translator_result_t r;
    translator_parser_t *GrammarTotal, *Grammar, *Term, *Factor, *Base;

    GrammarTotal = translator_new("grammar_total");
    Grammar = translator_new("grammar");
    Term = translator_new("term");
    Factor = translator_new("factor");
    Base = translator_new("base");

    translator_define(GrammarTotal,
                      translator_predictive(translator_total(Grammar, translator_soft_delete))
    );

    translator_define(Grammar, translator_and(2, translatoraf_grammar_or,
                                              Term,
                                              translator_maybe(
                                                      translator_and(2, translatorf_snd_free, translator_sym("|"),
                                                                     Grammar, free)),
                                              translator_soft_delete
    ));

    translator_define(Term, translator_many1(translatoraf_grammar_and, Factor));

    translator_define(Factor, translator_and(2, translatoraf_grammar_repeat,
                                             Base,
                                             translator_or(6,
                                                           translator_sym("*"),
                                                           translator_sym("+"),
                                                           translator_sym("?"),
                                                           translator_sym("!"),
                                                           translator_tok_brackets(translator_int(), free),
                                                           translator_pass()),
                                             translator_soft_delete
    ));

    translator_define(Base, translator_or(5,
                                          translator_apply_to(translator_tok(translator_string_lit()),
                                                              translatoraf_grammar_string, st),
                                          translator_apply_to(translator_tok(translator_char_lit()),
                                                              translatoraf_grammar_char, st),
                                          translator_apply_to(translator_tok(translator_regex_lit()),
                                                              translatoraf_grammar_regex, st),
                                          translator_apply_to(translator_tok_braces(
                                                  translator_or(2, translator_digits(), translator_ident()), free),
                                                              translatoraf_grammar_id, st),
                                          translator_tok_parens(Grammar, translator_soft_delete)
    ));

    translator_optimise(GrammarTotal);
    translator_optimise(Grammar);
    translator_optimise(Factor);
    translator_optimise(Term);
    translator_optimise(Base);

    if (!translator_parse("<translator_grammar_compiler>", grammar, GrammarTotal, &r)) {
        err_msg = translator_err_string(r.error);
        err_out = translator_failf("Invalid Grammar: %s", err_msg);
        translator_err_delete(r.error);
        free(err_msg);
        r.output = err_out;
    }

    translator_cleanup(5, GrammarTotal, Grammar, Term, Factor, Base);

    translator_optimise(r.output);

    return (st->flags & TRANSLATOR_LANG_PREDICTIVE) ? translator_predictive(r.output) : r.output;

}

translator_parser_t *translatora_grammar(int flags, const char *grammar, ...) {
    translatora_grammar_st_t st;
    translator_parser_t *res;
    va_list va;
    va_start(va, grammar);

    st.va = &va;
    st.parsers_num = 0;
    st.parsers = NULL;
    st.flags = flags;

    res = translatora_grammar_st(grammar, &st);
    free(st.parsers);
    va_end(va);
    return res;
}

typedef struct {
    char *ident;
    char *name;
    translator_parser_t *grammar;
} translatora_stmt_t;

static translator_val_t *translatora_stmt_afold(int n, translator_val_t **xs) {
    translatora_stmt_t *stmt = malloc(sizeof(translatora_stmt_t));
    stmt->ident = ((char **) xs)[0];
    stmt->name = ((char **) xs)[1];
    stmt->grammar = ((translator_parser_t **) xs)[3];
    (void) n;
    free(((char **) xs)[2]);
    free(((char **) xs)[4]);

    return stmt;
}

static translator_val_t *translatora_stmt_fold(int n, translator_val_t **xs) {

    int i;
    translatora_stmt_t **stmts = malloc(sizeof(translatora_stmt_t *) * (n + 1));

    for (i = 0; i < n; i++) {
        stmts[i] = xs[i];
    }
    stmts[n] = NULL;

    return stmts;
}

static void translatora_stmt_list_delete(translator_val_t *x) {

    translatora_stmt_t **stmts = x;

    while (*stmts) {
        translatora_stmt_t *stmt = *stmts;
        free(stmt->ident);
        free(stmt->name);
        translator_soft_delete(stmt->grammar);
        free(stmt);
        stmts++;
    }
    free(x);

}

static translator_val_t *translatora_stmt_list_apply_to(translator_val_t *x, void *s) {

    translatora_grammar_st_t *st = s;
    translatora_stmt_t *stmt;
    translatora_stmt_t **stmts = x;
    translator_parser_t *left;

    while (*stmts) {
        stmt = *stmts;
        left = translatora_grammar_find_parser(stmt->ident, st);
        if (st->flags & TRANSLATOR_LANG_PREDICTIVE) { stmt->grammar = translator_predictive(stmt->grammar); }
        if (stmt->name) { stmt->grammar = translator_expect(stmt->grammar, stmt->name); }
        translator_optimise(stmt->grammar);
        translator_define(left, stmt->grammar);
        free(stmt->ident);
        free(stmt->name);
        free(stmt);
        stmts++;
    }

    free(x);

    return NULL;
}

static translator_err_t *translatora_lang_st(translator_input_t *i, translatora_grammar_st_t *st) {

    translator_result_t r;
    translator_err_t *e;
    translator_parser_t *Lang, *Stmt, *Grammar, *Term, *Factor, *Base;

    Lang = translator_new("lang");
    Stmt = translator_new("stmt");
    Grammar = translator_new("grammar");
    Term = translator_new("term");
    Factor = translator_new("factor");
    Base = translator_new("base");

    translator_define(Lang, translator_apply_to(
            translator_total(translator_predictive(translator_many(translatora_stmt_fold, Stmt)),
                             translatora_stmt_list_delete),
            translatora_stmt_list_apply_to, st
    ));

    translator_define(Stmt, translator_and(5, translatora_stmt_afold,
                                           translator_tok(translator_ident()),
                                           translator_maybe(translator_tok(translator_string_lit())),
                                           translator_sym(":"), Grammar, translator_sym(";"),
                                           free, free, free, translator_soft_delete
    ));

    translator_define(Grammar, translator_and(2, translatoraf_grammar_or,
                                              Term,
                                              translator_maybe(
                                                      translator_and(2, translatorf_snd_free, translator_sym("|"),
                                                                     Grammar, free)),
                                              translator_soft_delete
    ));

    translator_define(Term, translator_many1(translatoraf_grammar_and, Factor));

    translator_define(Factor, translator_and(2, translatoraf_grammar_repeat,
                                             Base,
                                             translator_or(6,
                                                           translator_sym("*"),
                                                           translator_sym("+"),
                                                           translator_sym("?"),
                                                           translator_sym("!"),
                                                           translator_tok_brackets(translator_int(), free),
                                                           translator_pass()),
                                             translator_soft_delete
    ));

    translator_define(Base, translator_or(5,
                                          translator_apply_to(translator_tok(translator_string_lit()),
                                                              translatoraf_grammar_string, st),
                                          translator_apply_to(translator_tok(translator_char_lit()),
                                                              translatoraf_grammar_char, st),
                                          translator_apply_to(translator_tok(translator_regex_lit()),
                                                              translatoraf_grammar_regex, st),
                                          translator_apply_to(translator_tok_braces(
                                                  translator_or(2, translator_digits(), translator_ident()), free),
                                                              translatoraf_grammar_id, st),
                                          translator_tok_parens(Grammar, translator_soft_delete)
    ));

    translator_optimise(Lang);
    translator_optimise(Stmt);
    translator_optimise(Grammar);
    translator_optimise(Term);
    translator_optimise(Factor);
    translator_optimise(Base);

    if (!translator_parse_input(i, Lang, &r)) {
        e = r.error;
    } else {
        e = NULL;
    }

    translator_cleanup(6, Lang, Stmt, Grammar, Term, Factor, Base);

    return e;
}

translator_err_t *translatora_lang_file(int flags, FILE *f, ...) {
    translatora_grammar_st_t st;
    translator_input_t *i;
    translator_err_t *err;

    va_list va;
    va_start(va, f);

    st.va = &va;
    st.parsers_num = 0;
    st.parsers = NULL;
    st.flags = flags;

    i = translator_input_new_file("<translatora_lang_file>", f);
    err = translatora_lang_st(i, &st);
    translator_input_delete(i);

    free(st.parsers);
    va_end(va);
    return err;
}

translator_err_t *translatora_lang_pipe(int flags, FILE *p, ...) {
    translatora_grammar_st_t st;
    translator_input_t *i;
    translator_err_t *err;

    va_list va;
    va_start(va, p);

    st.va = &va;
    st.parsers_num = 0;
    st.parsers = NULL;
    st.flags = flags;

    i = translator_input_new_pipe("<translatora_lang_pipe>", p);
    err = translatora_lang_st(i, &st);
    translator_input_delete(i);

    free(st.parsers);
    va_end(va);
    return err;
}

translator_err_t *translatora_lang(int flags, const char *language, ...) {

    translatora_grammar_st_t st;
    translator_input_t *i;
    translator_err_t *err;

    va_list va;
    va_start(va, language);

    st.va = &va;
    st.parsers_num = 0;
    st.parsers = NULL;
    st.flags = flags;

    i = translator_input_new_string("<translatora_lang>", language);
    err = translatora_lang_st(i, &st);
    translator_input_delete(i);

    free(st.parsers);
    va_end(va);
    return err;
}

translator_err_t *translatora_lang_contents(int flags, const char *filename, ...) {

    translatora_grammar_st_t st;
    translator_input_t *i;
    translator_err_t *err;

    va_list va;

    FILE *f = fopen(filename, "rb");

    if (f == NULL) {
        err = translator_err_file(filename, "Unable to open file!");
        return err;
    }

    va_start(va, filename);

    st.va = &va;
    st.parsers_num = 0;
    st.parsers = NULL;
    st.flags = flags;

    i = translator_input_new_file(filename, f);
    err = translatora_lang_st(i, &st);
    translator_input_delete(i);

    free(st.parsers);
    va_end(va);

    fclose(f);

    return err;
}

static int translator_nodecount_unretained(translator_parser_t *p, int force) {

    int i, total;

    if (p->retained && !force) { return 0; }

    if (p->type == translator_TYPE_EXPECT) { return 1 + translator_nodecount_unretained(p->data.expect.x, 0); }

    if (p->type == translator_TYPE_APPLY) { return 1 + translator_nodecount_unretained(p->data.apply.x, 0); }
    if (p->type == translator_TYPE_APPLY_TO) { return 1 + translator_nodecount_unretained(p->data.apply_to.x, 0); }
    if (p->type == translator_TYPE_PREDICT) { return 1 + translator_nodecount_unretained(p->data.predict.x, 0); }

    if (p->type == translator_TYPE_NOT) { return 1 + translator_nodecount_unretained(p->data.not.x, 0); }
    if (p->type == translator_TYPE_MAYBE) { return 1 + translator_nodecount_unretained(p->data.not.x, 0); }

    if (p->type == translator_TYPE_MANY) { return 1 + translator_nodecount_unretained(p->data.repeat.x, 0); }
    if (p->type == translator_TYPE_MANY1) { return 1 + translator_nodecount_unretained(p->data.repeat.x, 0); }
    if (p->type == translator_TYPE_COUNT) { return 1 + translator_nodecount_unretained(p->data.repeat.x, 0); }

    if (p->type == translator_TYPE_OR) {
        total = 0;
        for (i = 0; i < p->data.or.n; i++) {
            total += translator_nodecount_unretained(p->data.or.xs[i], 0);
        }
        return total;
    }

    if (p->type == translator_TYPE_AND) {
        total = 0;
        for (i = 0; i < p->data.and.n; i++) {
            total += translator_nodecount_unretained(p->data.and.xs[i], 0);
        }
        return total;
    }

    return 1;

}

void translator_stats(translator_parser_t *p) {
    printf("Stats\n");
    printf("=====\n");
    printf("Node Count: %i\n", translator_nodecount_unretained(p, 1));
}

static void translator_optimise_unretained(translator_parser_t *p, int force) {

    int i, n, m;
    translator_parser_t *t;

    if (p->retained && !force) { return; }

    /* Optimise Subexpressions */

    if (p->type == translator_TYPE_EXPECT) { translator_optimise_unretained(p->data.expect.x, 0); }
    if (p->type == translator_TYPE_APPLY) { translator_optimise_unretained(p->data.apply.x, 0); }
    if (p->type == translator_TYPE_APPLY_TO) { translator_optimise_unretained(p->data.apply_to.x, 0); }
    if (p->type == translator_TYPE_PREDICT) { translator_optimise_unretained(p->data.predict.x, 0); }
    if (p->type == translator_TYPE_NOT) { translator_optimise_unretained(p->data.not.x, 0); }
    if (p->type == translator_TYPE_MAYBE) { translator_optimise_unretained(p->data.not.x, 0); }
    if (p->type == translator_TYPE_MANY) { translator_optimise_unretained(p->data.repeat.x, 0); }
    if (p->type == translator_TYPE_MANY1) { translator_optimise_unretained(p->data.repeat.x, 0); }
    if (p->type == translator_TYPE_COUNT) { translator_optimise_unretained(p->data.repeat.x, 0); }

    if (p->type == translator_TYPE_OR) {
        for (i = 0; i < p->data.or.n; i++) {
            translator_optimise_unretained(p->data.or.xs[i], 0);
        }
    }

    if (p->type == translator_TYPE_AND) {
        for (i = 0; i < p->data.and.n; i++) {
            translator_optimise_unretained(p->data.and.xs[i], 0);
        }
    }

    /* Perform optimisations */

    while (1) {

        /* Merge rhs `or` */
        if (p->type == translator_TYPE_OR
            && p->data.or.xs[p->data.or.n - 1]->type == translator_TYPE_OR
            && !p->data.or.xs[p->data.or.n - 1]->retained) {
            t = p->data.or.xs[p->data.or.n - 1];
            n = p->data.or.n;
            m = t->data.or.n;
            p->data.or.n = n + m - 1;
            p->data.or.xs = realloc(p->data.or.xs, sizeof(translator_parser_t *) * (n + m - 1));
            memmove(p->data.or.xs + n - 1, t->data.or.xs, m * sizeof(translator_parser_t *));
            free(t->data.or.xs);
            free(t->name);
            free(t);
            continue;
        }

        /* Merge lhs `or` */
        if (p->type == translator_TYPE_OR
            && p->data.or.xs[0]->type == translator_TYPE_OR
            && !p->data.or.xs[0]->retained) {
            t = p->data.or.xs[0];
            n = p->data.or.n;
            m = t->data.or.n;
            p->data.or.n = n + m - 1;
            p->data.or.xs = realloc(p->data.or.xs, sizeof(translator_parser_t *) * (n + m - 1));
            memmove(p->data.or.xs + m, t->data.or.xs + 1, n * sizeof(translator_parser_t *));
            memmove(p->data.or.xs, t->data.or.xs, m * sizeof(translator_parser_t *));
            free(t->data.or.xs);
            free(t->name);
            free(t);
            continue;
        }

        /* Remove ast `pass` */
        if (p->type == translator_TYPE_AND
            && p->data.and.n == 2
            && p->data.and.xs[0]->type == translator_TYPE_PASS
            && !p->data.and.xs[0]->retained
            && p->data.and.f == translatorf_fold_ast) {
            t = p->data.and.xs[1];
            translator_delete(p->data.and.xs[0]);
            free(p->data.and.xs);
            free(p->data.and.dxs);
            free(p->name);
            memcpy(p, t, sizeof(translator_parser_t));
            free(t);
            continue;
        }

        /* Merge ast lhs `and` */
        if (p->type == translator_TYPE_AND
            && p->data.and.f == translatorf_fold_ast
            && p->data.and.xs[0]->type == translator_TYPE_AND
            && !p->data.and.xs[0]->retained
            && p->data.and.xs[0]->data.and.f == translatorf_fold_ast) {
            t = p->data.and.xs[0];
            n = p->data.and.n;
            m = t->data.and.n;
            p->data.and.n = n + m - 1;
            p->data.and.xs = realloc(p->data.and.xs, sizeof(translator_parser_t *) * (n + m - 1));
            p->data.and.dxs = realloc(p->data.and.dxs, sizeof(translator_dtor_t) * (n + m - 1 - 1));
            memmove(p->data.and.xs + m, p->data.and.xs + 1, (n - 1) * sizeof(translator_parser_t *));
            memmove(p->data.and.xs, t->data.and.xs, m * sizeof(translator_parser_t *));
            for (i = 0; i < p->data.and.n - 1; i++) { p->data.and.dxs[i] = (translator_dtor_t) translator_ast_delete; }
            free(t->data.and.xs);
            free(t->data.and.dxs);
            free(t->name);
            free(t);
            continue;
        }

        /* Merge ast rhs `and` */
        if (p->type == translator_TYPE_AND
            && p->data.and.f == translatorf_fold_ast
            && p->data.and.xs[p->data.and.n - 1]->type == translator_TYPE_AND
            && !p->data.and.xs[p->data.and.n - 1]->retained
            && p->data.and.xs[p->data.and.n - 1]->data.and.f == translatorf_fold_ast) {
            t = p->data.and.xs[p->data.and.n - 1];
            n = p->data.and.n;
            m = t->data.and.n;
            p->data.and.n = n + m - 1;
            p->data.and.xs = realloc(p->data.and.xs, sizeof(translator_parser_t *) * (n + m - 1));
            p->data.and.dxs = realloc(p->data.and.dxs, sizeof(translator_dtor_t) * (n + m - 1 - 1));
            memmove(p->data.and.xs + n - 1, t->data.and.xs, m * sizeof(translator_parser_t *));
            for (i = 0; i < p->data.and.n - 1; i++) { p->data.and.dxs[i] = (translator_dtor_t) translator_ast_delete; }
            free(t->data.and.xs);
            free(t->data.and.dxs);
            free(t->name);
            free(t);
            continue;
        }

        /* Remove re `lift` */
        if (p->type == translator_TYPE_AND
            && p->data.and.n == 2
            && p->data.and.xs[0]->type == translator_TYPE_LIFT
            && p->data.and.xs[0]->data.lift.lf == translatorf_ctor_str
            && !p->data.and.xs[0]->retained
            && p->data.and.f == translatorf_strfold) {
            t = p->data.and.xs[1];
            translator_delete(p->data.and.xs[0]);
            free(p->data.and.xs);
            free(p->data.and.dxs);
            free(p->name);
            memcpy(p, t, sizeof(translator_parser_t));
            free(t);
            continue;
        }

        /* Merge re lhs `and` */
        if (p->type == translator_TYPE_AND
            && p->data.and.f == translatorf_strfold
            && p->data.and.xs[0]->type == translator_TYPE_AND
            && !p->data.and.xs[0]->retained
            && p->data.and.xs[0]->data.and.f == translatorf_strfold) {
            t = p->data.and.xs[0];
            n = p->data.and.n;
            m = t->data.and.n;
            p->data.and.n = n + m - 1;
            p->data.and.xs = realloc(p->data.and.xs, sizeof(translator_parser_t *) * (n + m - 1));
            p->data.and.dxs = realloc(p->data.and.dxs, sizeof(translator_dtor_t) * (n + m - 1 - 1));
            memmove(p->data.and.xs + m, p->data.and.xs + 1, (n - 1) * sizeof(translator_parser_t *));
            memmove(p->data.and.xs, t->data.and.xs, m * sizeof(translator_parser_t *));
            for (i = 0; i < p->data.and.n - 1; i++) { p->data.and.dxs[i] = free; }
            free(t->data.and.xs);
            free(t->data.and.dxs);
            free(t->name);
            free(t);
            continue;
        }

        /* Merge re rhs `and` */
        if (p->type == translator_TYPE_AND
            && p->data.and.f == translatorf_strfold
            && p->data.and.xs[p->data.and.n - 1]->type == translator_TYPE_AND
            && !p->data.and.xs[p->data.and.n - 1]->retained
            && p->data.and.xs[p->data.and.n - 1]->data.and.f == translatorf_strfold) {
            t = p->data.and.xs[p->data.and.n - 1];
            n = p->data.and.n;
            m = t->data.and.n;
            p->data.and.n = n + m - 1;
            p->data.and.xs = realloc(p->data.and.xs, sizeof(translator_parser_t *) * (n + m - 1));
            p->data.and.dxs = realloc(p->data.and.dxs, sizeof(translator_dtor_t) * (n + m - 1 - 1));
            memmove(p->data.and.xs + n - 1, t->data.and.xs, m * sizeof(translator_parser_t *));
            for (i = 0; i < p->data.and.n - 1; i++) { p->data.and.dxs[i] = free; }
            free(t->data.and.xs);
            free(t->data.and.dxs);
            free(t->name);
            free(t);
            continue;
        }

        return;

    }

}

void translator_optimise(translator_parser_t *p) {
    translator_optimise_unretained(p, 1);
}
