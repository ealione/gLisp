#include "ptest.h"
#include "../translator.h"

void test_grammar(void) {

  translator_parser_t *Expr, *Prod, *Value, *Maths;
  translator_ast_t *t0, *t1, *t2;
  
  Expr  = translator_new("expression");
  Prod  = translator_new("product");
  Value = translator_new("value");
  Maths = translator_new("maths");
  
  translator_define(Expr,  translatora_grammar(TRANSLATOR_LANG_DEFAULT, " <product> (('+' | '-') <product>)* ", Prod));
  translator_define(Prod,  translatora_grammar(TRANSLATOR_LANG_DEFAULT, " <value>   (('*' | '/')   <value>)* ", Value));
  translator_define(Value, translatora_grammar(TRANSLATOR_LANG_DEFAULT, " /[0-9]+/ | '(' <expression> ')' ", Expr));
  translator_define(Maths, translatora_total(Expr));
  
  t0 = translator_ast_new("product|value|regex", "24");
  t1 = translator_ast_build(1, "product|>",
    translator_ast_build(3, "value|>",
      translator_ast_new("char", "("),
      translator_ast_new("expression|product|value|regex", "5"),
      translator_ast_new("char", ")")));
  
  t2 = translator_ast_build(3, ">",
      
      translator_ast_build(3, "product|value|>",
        translator_ast_new("char", "("),
        translator_ast_build(3, "expression|>",
          
          translator_ast_build(5, "product|>", 
            translator_ast_new("value|regex", "4"),
            translator_ast_new("char", "*"),
            translator_ast_new("value|regex", "2"),
            translator_ast_new("char", "*"),
            translator_ast_new("value|regex", "11")),
            
          translator_ast_new("char", "+"),
          translator_ast_new("product|value|regex", "2")),
        translator_ast_new("char", ")")),
      
      translator_ast_new("char", "+"),
      translator_ast_new("product|value|regex", "5"));
  
  PT_ASSERT(translator_test_pass(Maths, "  24 ", t0, (int(*)(const void*,const void*))translator_ast_eq, (translator_dtor_t)translator_ast_delete, (void(*)(const void*))translator_ast_print));
  PT_ASSERT(translator_test_pass(Maths, "(5)", t1, (int(*)(const void*,const void*))translator_ast_eq, (translator_dtor_t)translator_ast_delete, (void(*)(const void*))translator_ast_print));
  PT_ASSERT(translator_test_pass(Maths, "(4 * 2 * 11 + 2) + 5", t2, (int(*)(const void*,const void*))translator_ast_eq, (translator_dtor_t)translator_ast_delete, (void(*)(const void*))translator_ast_print));
  PT_ASSERT(translator_test_fail(Maths, "a", t0, (int(*)(const void*,const void*))translator_ast_eq, (translator_dtor_t)translator_ast_delete, (void(*)(const void*))translator_ast_print));
  PT_ASSERT(translator_test_fail(Maths, "2b+4", t0, (int(*)(const void*,const void*))translator_ast_eq, (translator_dtor_t)translator_ast_delete, (void(*)(const void*))translator_ast_print));

  translator_ast_delete(t0);
  translator_ast_delete(t1);
  translator_ast_delete(t2);
  
  translator_cleanup(4, Expr, Prod, Value, Maths);
  
}

void test_language(void) {
  
  translator_parser_t *Expr, *Prod, *Value, *Maths;
  
  Expr  = translator_new("expression");
  Prod  = translator_new("product");
  Value = translator_new("value");
  Maths = translator_new("maths");
  
  translatora_lang(TRANSLATOR_LANG_DEFAULT,
    " expression : <product> (('+' | '-') <product>)*; "
    " product : <value>   (('*' | '/')   <value>)*;    "
    " value : /[0-9]+/ | '(' <expression> ')';         "
    " maths : /^/ <expression> /$/;                    ",
    Expr, Prod, Value, Maths);
  
  translator_cleanup(4, Expr, Prod, Value, Maths);
}

void test_language_file(void) {
  
  translator_parser_t *Expr, *Prod, *Value, *Maths;
  
  Expr  = translator_new("expression");
  Prod  = translator_new("product");
  Value = translator_new("value");
  Maths = translator_new("maths");
  
  translatora_lang_contents(TRANSLATOR_LANG_DEFAULT, "./tests/maths.grammar", Expr, Prod, Value, Maths);
  
  translator_cleanup(4, Expr, Prod, Value, Maths);
  
}

void test_doge(void) {
  
  translator_ast_t *t0;  
  translator_parser_t* Adjective = translator_new("adjective");
  translator_parser_t* Noun      = translator_new("noun");
  translator_parser_t* Phrase    = translator_new("phrase");
  translator_parser_t* Doge      = translator_new("doge");

  translatora_lang(TRANSLATOR_LANG_DEFAULT,
    " adjective : \"wow\" | \"many\" | \"so\" | \"such\";                 "
    " noun      : \"lisp\" | \"language\" | \"c\" | \"book\" | \"build\"; "
    " phrase    : <adjective> <noun>;                                     "
    " doge      : /^/ <phrase>* /$/;                                      ",
    Adjective, Noun, Phrase, Doge, NULL);
  
  t0 = 
      translator_ast_build(4, ">", 
          translator_ast_new("regex", ""),
          translator_ast_build(2, "phrase|>", 
            translator_ast_new("adjective|string", "so"),
            translator_ast_new("noun|string", "c")),
          translator_ast_build(2, "phrase|>", 
            translator_ast_new("adjective|string", "so"),
            translator_ast_new("noun|string", "c")),
          translator_ast_new("regex", "")
        );
            
  PT_ASSERT(translator_test_pass(Doge, "so c so c", t0, (int(*)(const void*,const void*))translator_ast_eq, (translator_dtor_t)translator_ast_delete, (void(*)(const void*))translator_ast_print));
 
  PT_ASSERT(translator_test_fail(Doge, "so a so c", t0, (int(*)(const void*,const void*))translator_ast_eq, (translator_dtor_t)translator_ast_delete, (void(*)(const void*))translator_ast_print));
  
  translator_ast_delete(t0);
  
  translator_cleanup(4, Adjective, Noun, Phrase, Doge);
  
}

void test_partial(void) {
  
  translator_ast_t *t0;
  translator_err_t *err;

  translator_parser_t *Line = translator_new("line");
  translator_parser_t *Number = translator_new("number");
  translator_parser_t *QuotedString = translator_new("quoted_string");
  translator_parser_t *LinePragma = translator_new("linepragma");
  translator_parser_t *Parser = translator_new("parser");
  
  translator_define(Line, translatora_tag(translator_apply(translator_sym("#line"), translatorf_str_ast), "string"));
  
  err = translatora_lang(TRANSLATOR_LANG_PREDICTIVE,
    "number        : /[0-9]+/ ;\n"
    "quoted_string : /\"(\\.|[^\"])*\"/ ;\n"
    "linepragma    : <line> <number> <quoted_string>;\n"
    "parser        : /^/ (<linepragma>)* /$/ ;\n",
    Line, Number, QuotedString, LinePragma, Parser, NULL);
  
  PT_ASSERT(err == NULL);
  
  t0 = translator_ast_build(3, ">", 
          translator_ast_new("regex", ""),
          translator_ast_build(3, "linepragma|>", 
            translator_ast_new("line|string", "#line"),
            translator_ast_new("number|regex", "10"),
            translator_ast_new("quoted_string|regex", "\"test\"")),
          translator_ast_new("regex", ""));
  
  PT_ASSERT(translator_test_pass(Parser, "#line 10 \"test\"", t0, 
    (int(*)(const void*,const void*))translator_ast_eq, 
    (translator_dtor_t)translator_ast_delete, 
    (void(*)(const void*))translator_ast_print));
  
  translator_cleanup(5, Line, Number, QuotedString, LinePragma, Parser);

}

void test_qscript(void) {
  
  translator_ast_t *t0;
  translator_parser_t *Qscript = translator_new("qscript");
  translator_parser_t *Comment = translator_new("comment");
  translator_parser_t *Resource = translator_new("resource");
  translator_parser_t *Rtype = translator_new("rtype");
  translator_parser_t *Rname = translator_new("rname");
  translator_parser_t *InnerBlock = translator_new("inner_block");
  translator_parser_t *Statement = translator_new("statement");
  translator_parser_t *Function = translator_new("function");
  translator_parser_t *Parameter = translator_new("parameter");
  translator_parser_t *Literal = translator_new("literal");
  translator_parser_t *Block = translator_new("block");
  translator_parser_t *Seperator = translator_new("seperator");
  translator_parser_t *Qstring = translator_new("qstring");
  translator_parser_t *SimpleStr = translator_new("simplestr");
  translator_parser_t *ComplexStr = translator_new("complexstr");
  translator_parser_t *Number = translator_new("number");
  translator_parser_t *Float = translator_new("float");
  translator_parser_t *Int = translator_new("int");
  
  translator_err_t *err = translatora_lang(0,
    "  qscript        : /^/ (<comment> | <resource>)* /$/ ;\n"
    "   comment     : '#' /[^\\n]*/ ;\n"
    "resource       : '[' (<rtype> <rname>) ']' <inner_block> ;\n"
    "   rtype       : /[*]*/ ;\n"
    "   rname       : <qstring> ;\n"
    "\n"
    "inner_block    : (<comment> | <statement>)* ;\n"
    "   statement   : <function> '(' (<comment> | <parameter> | <block>)* ')'  <seperator> ;\n"
    "   function    : <qstring> ;\n"
    "   parameter   : (<statement> | <literal>) ;\n"
    "      literal  : (<number> | <qstring>) <seperator> ;\n"
    "   block       : '{' <inner_block> '}' ;\n"
    "   seperator   : ',' | \"\" ;\n"
    "\n"
    "qstring        : (<complexstr> | <simplestr>) <qstring>* ;\n"
    "   simplestr   : /[a-zA-Z0-9_!@#$%^&\\*_+\\-\\.=\\/<>]+/ ;\n"
    "   complexstr  : (/\"[^\"]*\"/ | /'[^']*'/) ;\n"
    "\n"
    "number         : (<float> | <int>) ;\n"
    "   float       : /[-+]?[0-9]+\\.[0-9]+/ ;\n"
    "   int         : /[-+]?[0-9]+/ ;\n",
  Qscript, Comment, Resource, Rtype, Rname, InnerBlock, Statement, Function,
  Parameter, Literal, Block, Seperator, Qstring, SimpleStr, ComplexStr, Number,
  Float, Int, NULL);
 
  PT_ASSERT(err == NULL);
  
  t0 = translator_ast_build(3, ">",
          translator_ast_new("regex", ""),
          translator_ast_build(5, "resource|>",
            translator_ast_new("char", "["),
            translator_ast_new("rtype|regex", ""),
            translator_ast_new("rname|qstring|simplestr|regex", "my_func"),
            translator_ast_new("char", "]"),
            translator_ast_build(5, "inner_block|statement|>",
              translator_ast_new("function|qstring|simplestr|regex", "echo"),
              translator_ast_new("char", "("),
              translator_ast_build(2, "parameter|literal|>",
                translator_ast_build(2, "qstring|>",
                  translator_ast_new("simplestr|regex", "a"),
                  translator_ast_build(2, "qstring|>",
                    translator_ast_new("simplestr|regex", "b"),
                    translator_ast_new("qstring|simplestr|regex", "c")
                  )
                ),
                translator_ast_new("seperator|string", "")
              ),
              translator_ast_new("char", ")"),
              translator_ast_new("seperator|string", "")
            )
          ),
          translator_ast_new("regex", ""));
  
  PT_ASSERT(translator_test_pass(Qscript, "[my_func]\n  echo (a b c)\n", t0,
    (int(*)(const void*,const void*))translator_ast_eq,
    (translator_dtor_t)translator_ast_delete,
    (void(*)(const void*))translator_ast_print));
  
  translator_cleanup(18, Qscript, Comment, Resource, Rtype, Rname, InnerBlock,
  Statement, Function, Parameter, Literal, Block, Seperator, Qstring,
  SimpleStr, ComplexStr, Number, Float, Int);
  
}

void test_missingrule(void) {
  
  int result;
  translator_err_t *err;
  translator_result_t r;
  translator_parser_t *Parser = translator_new("parser");
  
  err = translatora_lang(TRANSLATOR_LANG_DEFAULT,
    "parser        : /^/ (<missing>)* /$/ ;\n",
    Parser, NULL);
  
  PT_ASSERT(err == NULL);
  
  result = translator_parse("<stdin>", "test", Parser, &r);
  
  PT_ASSERT(result == 0);
  PT_ASSERT(r.error != NULL);
  PT_ASSERT(strcmp(r.error->failure, "Unknown Parser 'missing'!") == 0);
  
  translator_err_delete(r.error);
  translator_cleanup(1, Parser);

}

void suite_grammar(void) {
  pt_add_test(test_grammar, "Test Grammar", "Suite Grammar");
  pt_add_test(test_language, "Test Language", "Suite Grammar");
  pt_add_test(test_language_file, "Test Language File", "Suite Grammar");
  pt_add_test(test_doge, "Test Doge", "Suite Grammar");
  pt_add_test(test_partial, "Test Partial", "Suite Grammar");
  pt_add_test(test_qscript, "Test QScript", "Suite Grammar");
  pt_add_test(test_missingrule, "Test Missing Rule", "Suite Grammar");
}
