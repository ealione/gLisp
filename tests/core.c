#include "ptest.h"
#include "../translator.h"

#include <stdlib.h>
#include <string.h>

static int int_eq(const void* x, const void* y) { return (*(int*)x == *(int*)y); }
static void int_print(const void* x) { printf("'%i'", *((int*)x)); }
static int streq(const void* x, const void* y) { return (strcmp(x, y) == 0); }
static void strprint(const void* x) { printf("'%s'", (char*)x); }

void test_ident(void) {

  /* ^[a-zA-Z_][a-zA-Z0-9_]*$ */
  
  translator_parser_t* Ident = translator_whole(
    translator_and(2, translatorf_strfold,
      translator_or(2, translator_alpha(), translator_underscore()),
      translator_many1(translatorf_strfold, translator_or(3, translator_alpha(), translator_underscore(), translator_digit())),
      free),
    free
  );
  
  PT_ASSERT(translator_test_pass(Ident, "test", "test", streq, free, strprint));
  PT_ASSERT(translator_test_fail(Ident, "  blah", "", streq, free, strprint));
  PT_ASSERT(translator_test_pass(Ident, "anoth21er", "anoth21er", streq, free, strprint));
  PT_ASSERT(translator_test_pass(Ident, "du__de", "du__de", streq, free, strprint));
  PT_ASSERT(translator_test_fail(Ident, "some spaces", "", streq, free, strprint));
  PT_ASSERT(translator_test_fail(Ident, "", "", streq, free, strprint));
  PT_ASSERT(translator_test_fail(Ident, "18nums", "", streq, free, strprint));
  
  translator_delete(Ident);

}

void test_maths(void) {
  
  translator_parser_t *Expr, *Factor, *Term, *Maths; 
  int r0 = 1, r1 = 5, r2 = 13, r3 = 0, r4 = 2;
  
  Expr   = translator_new("expr");
  Factor = translator_new("factor");
  Term   = translator_new("term");
  Maths  = translator_new("maths");

  translator_define(Expr, translator_or(2, 
    translator_and(3, translatorf_maths, Factor, translator_oneof("*/"), Factor, free, free),
    Factor
  ));
  
  translator_define(Factor, translator_or(2, 
    translator_and(3, translatorf_maths, Term, translator_oneof("+-"), Term, free, free),
    Term
  ));
  
  translator_define(Term, translator_or(2, 
    translator_int(),
    translator_parens(Expr, free)
  ));
  
  translator_define(Maths, translator_whole(Expr, free));
  
  PT_ASSERT(translator_test_pass(Maths, "1", &r0, int_eq, free, int_print));
  PT_ASSERT(translator_test_pass(Maths, "(5)", &r1, int_eq, free, int_print));
  PT_ASSERT(translator_test_pass(Maths, "(4*2)+5", &r2, int_eq, free, int_print));
  PT_ASSERT(translator_test_fail(Maths, "a", &r3, int_eq, free, int_print));
  PT_ASSERT(translator_test_fail(Maths, "2b+4", &r4, int_eq, free, int_print));
  
  translator_cleanup(4, Expr, Factor, Term, Maths);
}

void test_strip(void) {
  
  translator_parser_t *Stripperl = translator_apply(translator_many(translatorf_strfold, translator_any()), translatorf_strtriml);
  translator_parser_t *Stripperr = translator_apply(translator_many(translatorf_strfold, translator_any()), translatorf_strtrimr);
  translator_parser_t *Stripper  = translator_apply(translator_many(translatorf_strfold, translator_any()), translatorf_strtrim);
  
  PT_ASSERT(translator_test_pass(Stripperl, " asdmlm dasd  ", "asdmlm dasd  ", streq, free, strprint));
  PT_ASSERT(translator_test_pass(Stripperr, " asdmlm dasd  ", " asdmlm dasd", streq, free, strprint));
  PT_ASSERT(translator_test_pass(Stripper,  " asdmlm dasd  ", "asdmlm dasd", streq, free, strprint));
  
  translator_delete(Stripperl);
  translator_delete(Stripperr);
  translator_delete(Stripper);
  
}

void test_repeat(void) {
  
  int success;
  translator_result_t r;
  translator_parser_t *p = translator_count(3, translatorf_strfold, translator_digit(), free);
  
  success = translator_parse("test", "046", p, &r);
  PT_ASSERT(success);
  PT_ASSERT_STR_EQ(r.output, "046");
  free(r.output);
  
  success = translator_parse("test", "046aa", p, &r);
  PT_ASSERT(success);
  PT_ASSERT_STR_EQ(r.output, "046");
  free(r.output);
  
  success = translator_parse("test", "04632", p, &r);
  PT_ASSERT(success);
  PT_ASSERT_STR_EQ(r.output, "046");
  free(r.output);
  
  success = translator_parse("test", "04", p, &r);
  PT_ASSERT(!success);
  translator_err_delete(r.error);
  
  translator_delete(p);
  
}

void test_copy(void) {
  
  int success;
  translator_result_t r;
  translator_parser_t* p = translator_or(2, translator_char('a'), translator_char('b'));
  translator_parser_t* q = translator_and(2, translatorf_strfold, p, translator_copy(p), free);
  
  success = translator_parse("test", "aa", q, &r);
  PT_ASSERT(success);
  PT_ASSERT_STR_EQ(r.output, "aa");
  free(r.output);

  success = translator_parse("test", "bb", q, &r);
  PT_ASSERT(success);
  PT_ASSERT_STR_EQ(r.output, "bb");
  free(r.output);
  
  success = translator_parse("test", "ab", q, &r);
  PT_ASSERT(success);
  PT_ASSERT_STR_EQ(r.output, "ab");
  free(r.output);
  
  success = translator_parse("test", "ba", q, &r);
  PT_ASSERT(success);
  PT_ASSERT_STR_EQ(r.output, "ba");
  free(r.output);
  
  success = translator_parse("test", "c", p, &r);
  PT_ASSERT(!success);
  translator_err_delete(r.error);
  
  translator_delete(translator_copy(p));
  translator_delete(translator_copy(q));
  
  translator_delete(q);
  
}

void suite_core(void) {
  pt_add_test(test_ident,  "Test Ident",  "Suite Core");
  pt_add_test(test_maths,  "Test Maths",  "Suite Core");
  pt_add_test(test_strip,  "Test Strip",  "Suite Core");
  pt_add_test(test_repeat, "Test Repeat", "Suite Core");
  pt_add_test(test_copy,   "Test Copy",   "Suite Core");
}
